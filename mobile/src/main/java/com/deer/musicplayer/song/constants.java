package com.deer.musicplayer.song;

import java.util.Random;

/**
 * Created by Usman Jamil on 02/02/2017.
 * Usmans.net
 * Skype usman.jamil78
 * email usmanjamil547@gmail.com
 */
public interface constants {

    String URL_FORMAT_SUGESSTION = "http://suggestqueries.google.com/complete/search?q=%1$s&client=firefox&hl=fr";

    String FORMAT_URI = "content://media/external/audio/media/%1$s";

    String URL_SEARCH_API = "https://api.soundcloud.com/tracks";

    String[] SOUNDCLOUND_CLIENT_ID2 = {"a3e059563d7fd3372b49b37f00a00bcf", "cUa40O3Jg3Emvp6Tv4U6ymYYO50NUGpJ", "fDoItMDbsbZz8dY16ZzARCZmzgHBPotA"};

    String DOWNLOAD_DIRECTORY = "/DeerMusicPlayer";

    String SEARCH_ID = "SEARCH";
    String TOPHIT_VID = "TOPHIT_VPOP";
    String TOPHIT_KID = "TOPHIT_KPOP";
    String TOPHIT_USID = "TOPHIT_USPOP";

    Random rn = new Random();
    int range2 =2;
    int randomNum2 =  rn.nextInt(range2);
    String CLIENT_ID2 = SOUNDCLOUND_CLIENT_ID2[randomNum2];

}
