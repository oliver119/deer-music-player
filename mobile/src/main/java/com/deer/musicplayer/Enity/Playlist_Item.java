package com.deer.musicplayer.Enity;

/**
 * Created by User on 8/10/2017.
 */

public class Playlist_Item {
    private String playlist_name;
    private String Image_url;

    public String getPlaylist_name() {
        return playlist_name;
    }

    public void setPlaylist_name(String playlist_name) {
        this.playlist_name = playlist_name;
    }

    public String getImage_url() {
        return Image_url;
    }

    public void setImage_url(String image_url) {
        Image_url = image_url;
    }
}
