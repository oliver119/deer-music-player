package com.deer.musicplayer.ui;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.deer.musicplayer.Dataset.Add_Dataset;
import com.deer.musicplayer.R;
import com.deer.musicplayer.utils.MediaIDHelper;

/**
 * Created by User on 8/11/2017.
 */

public class AddPlaylistActivity extends Activity {
    private TextView btn_add, btn_cancel;
    private EditText edit_add;
    private Add_Dataset add_dataset = new Add_Dataset();
    Bundle extras;
    String name, allbum, artist, icon, duration, source, song_id;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.addplaylist_activity);

        DisplayMetrics dm = new DisplayMetrics();

        getWindowManager().getDefaultDisplay().getMetrics(dm);
        int w = dm.widthPixels;
        int h = dm.heightPixels;

        getWindow().setLayout((int) (w * .7), (int) (h * .2));


        initView();


        if (savedInstanceState == null) {
            extras = getIntent().getExtras();
            if (extras == null) {
                name = null;
                allbum = null;
                artist = null;
                duration = null;
                icon = null;
                source = null;
                song_id = null;


            } else {


                name = extras.getString(MediaIDHelper.NAME_SONG);
                allbum = extras.getString(MediaIDHelper.ALLBUM_SONG);
                artist = extras.getString(MediaIDHelper.ARTIST_SONG);
                duration = extras.getString(MediaIDHelper.DURATION_SONG);
                icon = extras.getString(MediaIDHelper.ICON_SONG);
                source = extras.getString(MediaIDHelper.SOURCE_SONG);
                song_id = extras.getString(MediaIDHelper.ID_SONG);

            }
        } else {
            name = (String) savedInstanceState.getSerializable(MediaIDHelper.NAME_SONG);
            allbum = (String) savedInstanceState.getSerializable(MediaIDHelper.ALLBUM_SONG);
            artist = (String) savedInstanceState.getSerializable(MediaIDHelper.ARTIST_SONG);
            duration = (String) savedInstanceState.getSerializable(MediaIDHelper.DURATION_SONG);
            icon = (String) savedInstanceState.getSerializable(MediaIDHelper.ICON_SONG);
            source = (String) savedInstanceState.getSerializable(MediaIDHelper.SOURCE_SONG);
            song_id = (String) savedInstanceState.getSerializable(MediaIDHelper.ID_SONG);

        }
    }


    private void initView() {
        btn_add = (TextView) findViewById(R.id.btn_addplaylist);
        btn_cancel = (TextView) findViewById(R.id.Cancel_playlist);

        edit_add = (EditText) findViewById(R.id.edit_playlist);

        btn_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                AlertDialog.Builder builder = new AlertDialog.Builder(AddPlaylistActivity.this);

                builder.setTitle("Confirm");
                builder.setMessage("Do you want add " + name + " song to " + edit_add.getText().toString() + " Playlist ?");


                builder.setPositiveButton("YES", new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int which) {


                        add_dataset.add_playlist(edit_add.getText().toString(), song_id, name, duration, allbum, artist, source, icon);

                        dialog.dismiss();

                        Toast.makeText(AddPlaylistActivity.this, "Added Successfully", Toast.LENGTH_SHORT).show();
                        finish();

                    }
                });

                builder.setNegativeButton("NO", new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {


                        dialog.dismiss();
                    }
                });

                AlertDialog alert = builder.create();
                alert.show();

            }
        });

        btn_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });


    }
}
