package com.deer.musicplayer.ui;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Fragment;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentActivity;
import android.support.v4.media.MediaBrowserCompat;
import android.support.v4.media.MediaMetadataCompat;
import android.support.v4.media.session.MediaControllerCompat;
import android.support.v4.media.session.PlaybackStateCompat;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.deer.musicplayer.Dataset.Delete_Dataset;
import com.deer.musicplayer.R;
import com.deer.musicplayer.utils.LogHelper;
import com.deer.musicplayer.utils.MediaIDHelper;
import com.deer.musicplayer.utils.NetworkHelper;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by User on 8/8/2017.
 */

public class PlaylistFragment extends Fragment {

    private static final String TAG = LogHelper.makeLogTag(MediaBrowserFragment.class);

    private static final String ARG_MEDIA_ID = "media_id";
    ProgressBar loading;

    private String checktype = "";
    private int Position;

    private MediaBrowserFragment.BrowseAdapter mBrowserAdapter;
    private String mMediaId;
    private MediaBrowserFragment.MediaFragmentListener mMediaFragmentListener;
    private View mErrorView;
    private TextView mErrorMessage;
    private final BroadcastReceiver mConnectivityChangeReceiver = new BroadcastReceiver() {
        private boolean oldOnline = false;

        @Override
        public void onReceive(Context context, Intent intent) {
            // We don't care about network changes while this fragment is not associated
            // with a media ID (for example, while it is being initialized)
            if (mMediaId != null) {
                boolean isOnline = NetworkHelper.isOnline(context);
                if (isOnline != oldOnline) {
                    oldOnline = isOnline;
                    checkForUserVisibleErrors(false);
                    if (isOnline) {
                        mBrowserAdapter.notifyDataSetChanged();
                    }
                }
            }
        }
    };

    // Receive callbacks from the MediaController. Here we update our state such as which queue
    // is being shown, the current title and description and the PlaybackState.
    private final MediaControllerCompat.Callback mMediaControllerCallback =
            new MediaControllerCompat.Callback() {
                @Override
                public void onMetadataChanged(MediaMetadataCompat metadata) {
                    super.onMetadataChanged(metadata);
                    if (metadata == null) {
                        return;
                    }
                    LogHelper.d(TAG, "Received metadata change to media ",
                            metadata.getDescription().getMediaId());
                    mBrowserAdapter.notifyDataSetChanged();
                }

                @Override
                public void onPlaybackStateChanged(@NonNull PlaybackStateCompat state) {
                    super.onPlaybackStateChanged(state);
                    LogHelper.d(TAG, "Received state change: ", state);
                    checkForUserVisibleErrors(false);
                    mBrowserAdapter.notifyDataSetChanged();
                }
            };

    private final MediaBrowserCompat.SubscriptionCallback mSubscriptionCallback =
            new MediaBrowserCompat.SubscriptionCallback() {
                @Override
                public void onChildrenLoaded(@NonNull String parentId,
                                             @NonNull List<MediaBrowserCompat.MediaItem> children) {
                    try {
                        LogHelper.d(TAG, "fragment onChildrenLoaded, parentId=" + parentId +
                                "  count=" + children.size());

                        checktype = parentId;
                        checkForUserVisibleErrors(children.isEmpty());
                        mBrowserAdapter.clear();
                        for (MediaBrowserCompat.MediaItem item : children) {
                            mBrowserAdapter.add(item);
                        }
                        mBrowserAdapter.notifyDataSetChanged();

                        loading.setVisibility(View.GONE);

                    } catch (Throwable t) {
                        LogHelper.e(TAG, "Error on childrenloaded", t);
                        loading.setVisibility(View.GONE);
                    }
                }

                @Override
                public void onError(@NonNull String id) {
                    LogHelper.e(TAG, "browse fragment subscription onError, id=" + id);
                    Toast.makeText(getActivity(), R.string.error_loading_media, Toast.LENGTH_LONG).show();
                    checkForUserVisibleErrors(true);
                    loading.setVisibility(View.GONE);
                }
            };

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        // If used on an activity that doesn't implement MediaFragmentListener, it
        // will throw an exception as expected:
        mMediaFragmentListener = (MediaBrowserFragment.MediaFragmentListener) activity;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        LogHelper.d(TAG, "fragment.onCreateView");
        View rootView = inflater.inflate(R.layout.fragment_list, container, false);

        mErrorView = rootView.findViewById(R.id.playback_error);
        mErrorMessage = (TextView) mErrorView.findViewById(R.id.error_message);
        loading = (ProgressBar) rootView.findViewById(R.id.progressBar);

        mBrowserAdapter = new MediaBrowserFragment.BrowseAdapter(getActivity());

        ListView listView = (ListView) rootView.findViewById(R.id.list_view);
        listView.setAdapter(mBrowserAdapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                checkForUserVisibleErrors(false);
                MediaBrowserCompat.MediaItem item = mBrowserAdapter.getItem(position);
                mMediaFragmentListener.onMediaItemSelected(item);
            }
        });

        registerForContextMenu(listView);

        return rootView;
    }

    @Override
    public void onStart() {
        super.onStart();

        // fetch browsing information to fill the listview:
        MediaBrowserCompat mediaBrowser = mMediaFragmentListener.getMediaBrowser();

        LogHelper.d(TAG, "fragment.onStart, mediaId=", mMediaId,
                "  onConnected=" + mediaBrowser.isConnected());

        if (mediaBrowser.isConnected()) {
            onConnected();
        }

        // Registers BroadcastReceiver to track network connection changes.
        this.getActivity().registerReceiver(mConnectivityChangeReceiver,
                new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
    }

    @Override
    public void onStop() {
        super.onStop();
        MediaBrowserCompat mediaBrowser = mMediaFragmentListener.getMediaBrowser();
        if (mediaBrowser != null && mediaBrowser.isConnected() && mMediaId != null) {
            mediaBrowser.unsubscribe(mMediaId);
        }
        MediaControllerCompat controller = ((FragmentActivity) getActivity())
                .getSupportMediaController();
        if (controller != null) {
            controller.unregisterCallback(mMediaControllerCallback);
        }
        this.getActivity().unregisterReceiver(mConnectivityChangeReceiver);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mMediaFragmentListener = null;
    }

    public String getMediaId() {
        Bundle args = getArguments();
        if (args != null) {
            return args.getString(ARG_MEDIA_ID);
        }
        return null;
    }

    public void setMediaId(String mediaId) {
        Bundle args = new Bundle(1);
        args.putString(PlaylistFragment.ARG_MEDIA_ID, mediaId);
        setArguments(args);
    }

    // Called when the MediaBrowser is connected. This method is either called by the
    // fragment.onStart() or explicitly by the activity in the case where the connection
    // completes after the onStart()
    public void onConnected() {
        if (isDetached()) {
            return;
        }
        mMediaId = getMediaId();
        if (mMediaId == null) {
            mMediaId = MediaIDHelper.MEDIA_ID_MUSICS_BY_PLAYLIST_NAME;
        }
        updateTitle();

        // Unsubscribing before subscribing is required if this mediaId already has a subscriber
        // on this MediaBrowser instance. Subscribing to an already subscribed mediaId will replace
        // the callback, but won't trigger the initial callback.onChildrenLoaded.
        //
        // This is temporary: A bug is being fixed that will make subscribe
        // consistently call onChildrenLoaded initially, no matter if it is replacing an existing
        // subscriber or not. Currently this only happens if the mediaID has no previous
        // subscriber or if the media content changes on the service side, so we need to
        // unsubscribe first.
        mMediaFragmentListener.getMediaBrowser().unsubscribe(mMediaId);
        loading.setVisibility(View.VISIBLE);

        mMediaFragmentListener.getMediaBrowser().subscribe(mMediaId, mSubscriptionCallback);


        // Add MediaController callback so we can redraw the list when metadata changes:
        MediaControllerCompat controller = ((FragmentActivity) getActivity())
                .getSupportMediaController();
        if (controller != null) {
            controller.registerCallback(mMediaControllerCallback);
        }
    }


    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        if (v.getId() == R.id.list_view) {
            MenuInflater inflater = getActivity().getMenuInflater();

            inflater.inflate(R.menu.item_delete_playlist, menu);


            if (checktype.equals("PLAYLIST_NAME")) {
                MenuItem moveMenuItem = menu.findItem(R.id.move_playitem);
                moveMenuItem.setVisible(false);

            } else {
                MenuItem moveMenuItem = menu.findItem(R.id.move_playitem);
                moveMenuItem.setVisible(true);
            }


        }

    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
        int position = info.position;

        final MediaBrowserCompat.MediaItem event = mBrowserAdapter.getItem(position);

        switch (item.getItemId()) {
            case R.id.move_playitem:

                Intent i = new Intent(getActivity(), MovePlaylistActivity.class);

                try {
                    if (event.getDescription().getSubtitle() == null) {
                        i.putExtra(MediaIDHelper.NAME_SONG, event.getDescription().getTitle());
                        i.putExtra(MediaIDHelper.ICON_SONG, event.getDescription().getIconUri().toString());
                        i.putExtra(MediaIDHelper.DURATION_SONG, String.valueOf(event.getDescription().getExtras().getLong(MediaMetadataCompat.METADATA_KEY_DURATION)));
                        i.putExtra(MediaIDHelper.SOURCE_SONG, event.getDescription().getExtras().getString(MediaIDHelper.SOURCE_SONG));
                        i.putExtra(MediaIDHelper.ARTIST_SONG, "<unknow>");
                        i.putExtra(MediaIDHelper.ALLBUM_SONG, "<unknow>");
                        i.putExtra(MediaIDHelper.ID_SONG, event.getDescription().getExtras().getString(MediaIDHelper.ID_SONG));
                        i.putExtra(MediaIDHelper.GENRE_SONG, event.getDescription().getExtras().getString(MediaIDHelper.GENRE_SONG));

                    } else {
                        i.putExtra(MediaIDHelper.NAME_SONG, event.getDescription().getTitle());
                        i.putExtra(MediaIDHelper.ICON_SONG, event.getDescription().getIconUri().toString());
                        i.putExtra(MediaIDHelper.DURATION_SONG, String.valueOf(event.getDescription().getExtras().getLong(MediaMetadataCompat.METADATA_KEY_DURATION)));
                        i.putExtra(MediaIDHelper.SOURCE_SONG, event.getDescription().getExtras().getString(MediaIDHelper.SOURCE_SONG));
                        i.putExtra(MediaIDHelper.ARTIST_SONG, event.getDescription().getSubtitle().toString());
                        i.putExtra(MediaIDHelper.ALLBUM_SONG, event.getDescription().getSubtitle().toString());
                        i.putExtra(MediaIDHelper.ID_SONG, event.getDescription().getExtras().getString(MediaIDHelper.ID_SONG));
                        i.putExtra(MediaIDHelper.GENRE_SONG, event.getDescription().getExtras().getString(MediaIDHelper.GENRE_SONG));
                    }


                } catch (NullPointerException e) {

                }

                Position = position;
                startActivityForResult(i, 1);


                return true;


            case R.id.delete_item:

                if (checktype.equals("PLAYLIST_NAME")) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

                    builder.setTitle("Confirm");
                    builder.setMessage("Do you want delete " + event.getDescription().getTitle() + " Playlist ?");

                    builder.setPositiveButton("YES", new DialogInterface.OnClickListener() {

                        public void onClick(DialogInterface dialog, int which) {

                            Delete_Dataset delete_dataset = new Delete_Dataset();
                            delete_dataset.delete_Playlist(event.getDescription().getTitle().toString());
                            dialog.dismiss();

                            mBrowserAdapter.remove(event);
                            mBrowserAdapter.notifyDataSetChanged();


                            Toast.makeText(getActivity(), "Deleted Successfully", Toast.LENGTH_SHORT).show();

                        }
                    });

                    builder.setNegativeButton("NO", new DialogInterface.OnClickListener() {

                        @Override
                        public void onClick(DialogInterface dialog, int which) {


                            dialog.dismiss();
                        }
                    });

                    AlertDialog alert = builder.create();
                    alert.show();

                } else {
                    AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

                    builder.setTitle("Confirm");
                    builder.setMessage("Do you want delete " + event.getDescription().getTitle() + " Song ?");

                    builder.setPositiveButton("YES", new DialogInterface.OnClickListener() {

                        public void onClick(DialogInterface dialog, int which) {

                            Delete_Dataset delete_dataset = new Delete_Dataset();
                            delete_dataset.deleteItem_Playlist(event.getDescription().getExtras().getString(MediaIDHelper.GENRE_SONG).toString(), event.getDescription().getExtras().getString(MediaIDHelper.ID_SONG).toString());
                            dialog.dismiss();

                            mBrowserAdapter.remove(event);
                            mBrowserAdapter.notifyDataSetChanged();


                            Toast.makeText(getActivity(), "Deleted Successfully", Toast.LENGTH_SHORT).show();

                        }
                    });

                    builder.setNegativeButton("NO", new DialogInterface.OnClickListener() {

                        @Override
                        public void onClick(DialogInterface dialog, int which) {


                            dialog.dismiss();
                        }
                    });

                    AlertDialog alert = builder.create();
                    alert.show();
                }


                return true;


            default:
                return super.onContextItemSelected(item);
        }

    }

    private void checkForUserVisibleErrors(boolean forceError) {
        boolean showError = forceError;
        // If offline, message is about the lack of connectivity:
        if (!NetworkHelper.isOnline(getActivity())) {
            mErrorMessage.setText(R.string.error_no_connection);
            showError = true;
        } else {
            // otherwise, if state is ERROR and metadata!=null, use playback state error message:
            MediaControllerCompat controller = ((FragmentActivity) getActivity())
                    .getSupportMediaController();
            if (controller != null
                    && controller.getMetadata() != null
                    && controller.getPlaybackState() != null
                    && controller.getPlaybackState().getState() == PlaybackStateCompat.STATE_ERROR
                    && controller.getPlaybackState().getErrorMessage() != null) {
                mErrorMessage.setText(controller.getPlaybackState().getErrorMessage());
                showError = true;
            } else if (forceError) {
                // Finally, if the caller requested to show error, show a generic message:
                mErrorMessage.setText(R.string.error_loading_media);
                showError = true;
            }
        }
        mErrorView.setVisibility(showError ? View.VISIBLE : View.GONE);
        LogHelper.d(TAG, "checkForUserVisibleErrors. forceError=", forceError,
                " showError=", showError,
                " isOnline=", NetworkHelper.isOnline(getActivity()));
    }

    private void updateTitle() {
        if (MediaIDHelper.MEDIA_ID_ROOT.equals(mMediaId)) {
            mMediaFragmentListener.setToolbarTitle(null);
            return;
        }

        MediaBrowserCompat mediaBrowser = mMediaFragmentListener.getMediaBrowser();
        mediaBrowser.getItem(mMediaId, new MediaBrowserCompat.ItemCallback() {
            @Override
            public void onItemLoaded(MediaBrowserCompat.MediaItem item) {
                mMediaFragmentListener.setToolbarTitle(
                        item.getDescription().getTitle());
            }
        });
    }

    // An adapter for showing the list of browsed MediaItem's
    public static class BrowseAdapter extends ArrayAdapter<MediaBrowserCompat.MediaItem> {

        public BrowseAdapter(Activity context) {
            super(context, R.layout.media_list_item, new ArrayList<MediaBrowserCompat.MediaItem>());
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            MediaBrowserCompat.MediaItem item = getItem(position);
            return MediaItemViewHolder.setupListView((Activity) getContext(), convertView, parent,
                    item);
        }
    }

    public interface MediaFragmentListener extends MediaBrowserProvider {
        void onMediaItemSelected(MediaBrowserCompat.MediaItem item);

        void setToolbarTitle(CharSequence title);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        MediaBrowserCompat.MediaItem event = mBrowserAdapter.getItem(Position);
        if (resultCode == MediaIDHelper.RESULT_OK) {

            mBrowserAdapter.remove(event);

            mBrowserAdapter.notifyDataSetChanged();

        } else {

        }


    }
}