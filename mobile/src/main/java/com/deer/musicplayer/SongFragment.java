package com.deer.musicplayer;
/**
 * Created by Usman Jamil on 02/02/2017.
 * Usmans.net
 * Skype usman.jamil78
 * email usmanjamil547@gmail.com
 */

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DownloadManager;
import android.app.Fragment;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.MatrixCursor;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.BaseColumns;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentActivity;
import android.support.v4.media.MediaBrowserCompat;
import android.support.v4.media.MediaMetadataCompat;
import android.support.v4.media.session.MediaControllerCompat;
import android.support.v4.media.session.PlaybackStateCompat;
import android.support.v4.widget.CursorAdapter;
import android.support.v4.widget.SimpleCursorAdapter;
import android.support.v7.widget.SearchView;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.cjj.MaterialRefreshLayout;
import com.deer.musicplayer.song.JSONParser;
import com.deer.musicplayer.song.constants;
import com.deer.musicplayer.ui.CreatePlaylistActivity;
import com.deer.musicplayer.ui.MediaBrowserProvider;
import com.deer.musicplayer.ui.MediaItemViewHolder;
import com.deer.musicplayer.utils.MediaIDHelper;
import com.deer.musicplayer.utils.NetworkHelper;

import org.json.JSONArray;

import java.util.ArrayList;
import java.util.List;


public class SongFragment extends Fragment implements constants {
    ListView lv;
    RelativeLayout NothingToShow;
    ProgressBar loading;


    private String mMediaId;
    private SongFragment.MediaFragmentListener mMediaFragmentListener;

    private List<MediaBrowserCompat.MediaItem> songList = new ArrayList<MediaBrowserCompat.MediaItem>();
    private BrowseAdapter adapter;
    int offset =0;


    private SearchView mSearchView;
    String[] Final_Suggestions=null;
    private SimpleCursorAdapter mAdapter;
    String SearchText=null;
    MaterialRefreshLayout materialRefreshLayout;
    View view;
    private long downloadID;
    private DownloadManager downloadManager;

    private Session session;
    Boolean Is = false;
  // Session session = new Session(getActivity());

    private static final String ARG_MEDIA_ID = "media_id";
    public SongFragment() {
        // Required empty public constructor
    }
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        session = new Session(getActivity());
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_one, container, false);

        lv = (ListView) view.findViewById(R.id.lvVideo);
        NothingToShow = (RelativeLayout)view.findViewById(R.id.NothingToShow);
        loading = (ProgressBar)view.findViewById(R.id.progressBar);

          adapter = new BrowseAdapter(getActivity());
//        materialRefreshLayout = (MaterialRefreshLayout) view.findViewById(R.id.refresh);
//        materialRefreshLayout.finishRefresh();
    //    materialRefreshLayout.autoRefreshLoadMore();// pull up refresh automatically

//        materialRefreshLayout.setMaterialRefreshListener(new MaterialRefreshListener() {
//            @Override
//            public void onRefresh(final MaterialRefreshLayout materialRefreshLayout) {
//                materialRefreshLayout.finishRefresh();
//            }
//
//            @Override
//            public void onRefreshLoadMore(MaterialRefreshLayout materialRefreshLayout) {
//              //  LoadMore(SearchText);
//            }
//        });

                setHasOptionsMenu(true);

        final String[] from = new String[] {"cityName"};
        final int[] to = new int[] {R.id.suggest};
        mAdapter = new SimpleCursorAdapter(getActivity(),
                R.layout.item_suggestion,
                null,
                from,
                to,
                CursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER);

        lv.setAdapter(adapter);

        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                MediaBrowserCompat.MediaItem item = adapter.getItem(position);
                mMediaFragmentListener.onMediaItemSelected(item);
            }
        });

        registerForContextMenu(lv);

        //  lv.setOnItemLongClickListener();


return  view;

    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
        int position = info.position;

        MediaBrowserCompat.MediaItem event = adapter.getItem(position);

        switch (item.getItemId()) {
            case R.id.down_item:

                String cutTitle = event.getDescription().getTitle().toString();
                cutTitle = cutTitle.replace(" ", "-").replace(".", "-") + ".mp3";
                downloadManager = (DownloadManager) getActivity().getSystemService(Context.DOWNLOAD_SERVICE);
                DownloadManager.Request request = new DownloadManager.Request(Uri.parse(event.getDescription().getExtras().getString(MediaIDHelper.SOURCE_SONG)));
                request.setTitle(event.getDescription().getTitle().toString());
                request.setDescription("Downloading");
                request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
                request.setDestinationInExternalPublicDir(DOWNLOAD_DIRECTORY, cutTitle);
                request.allowScanningByMediaScanner();

                downloadID = downloadManager.enqueue(request);


                Toast.makeText(getActivity(), "Downloading Start", Toast.LENGTH_SHORT).show();



                return true;

            case R.id.playitem:



                // Toast.makeText(view.getContext(), String.valueOf(event.getDescription().getExtras().getLong(MediaMetadataCompat.METADATA_KEY_DURATION)), Toast.LENGTH_SHORT).show();


                Intent i = new Intent(getActivity(), CreatePlaylistActivity.class);

                try {
                    if (event.getDescription().getSubtitle() == null) {
                        i.putExtra(MediaIDHelper.NAME_SONG, event.getDescription().getTitle());
                        i.putExtra(MediaIDHelper.ICON_SONG, event.getDescription().getIconUri().toString());
                        i.putExtra(MediaIDHelper.DURATION_SONG, String.valueOf(event.getDescription().getExtras().getLong(MediaMetadataCompat.METADATA_KEY_DURATION)));
                        i.putExtra(MediaIDHelper.SOURCE_SONG, event.getDescription().getExtras().getString(MediaIDHelper.SOURCE_SONG));
                        i.putExtra(MediaIDHelper.ARTIST_SONG, "<unknow>");
                        i.putExtra(MediaIDHelper.ALLBUM_SONG, "<unknow>");
                        i.putExtra(MediaIDHelper.ID_SONG, event.getDescription().getExtras().getString(MediaIDHelper.ID_SONG));
                    } else {
                        i.putExtra(MediaIDHelper.NAME_SONG, event.getDescription().getTitle());
                        i.putExtra(MediaIDHelper.ICON_SONG, event.getDescription().getIconUri().toString());
                        i.putExtra(MediaIDHelper.DURATION_SONG, String.valueOf(event.getDescription().getExtras().getLong(MediaMetadataCompat.METADATA_KEY_DURATION)));
                        i.putExtra(MediaIDHelper.SOURCE_SONG, event.getDescription().getExtras().getString(MediaIDHelper.SOURCE_SONG));
                        i.putExtra(MediaIDHelper.ARTIST_SONG, event.getDescription().getSubtitle().toString());
                        i.putExtra(MediaIDHelper.ALLBUM_SONG, event.getDescription().getSubtitle().toString());
                        i.putExtra(MediaIDHelper.ID_SONG, event.getDescription().getExtras().getString(MediaIDHelper.ID_SONG));
                    }


                } catch (NullPointerException e) {

                }



                startActivity(i);


                return true;



            default:
                return super.onContextItemSelected(item);
        }


    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        if (v.getId() == R.id.lvVideo) {
            MenuInflater inflater = getActivity().getMenuInflater();

            inflater.inflate(R.menu.item_menu, menu);

        }


    }

    public void search(String text){


        if (JSONParser.isNetworkAvailable(getActivity())) {
            loading.setVisibility(View.VISIBLE);
            NothingToShow.setVisibility(View.INVISIBLE);

            String Furl=URL_SEARCH_API+"?client_id="+CLIENT_ID2+"&q="+ Uri.encode(text,"UTF-8")+"&limit=200";



            session.set_search_key(Furl);


            //((PlaceholderActivity)getActivity()).refesh();
            mMediaFragmentListener.getMediaBrowser().unsubscribe(mMediaId);

            mMediaFragmentListener.getMediaBrowser().subscribe(mMediaId, mSubscriptionCallback);
            lv.setAdapter(adapter);



           // new HttpAsyncTaskSearch().execute(Furl);
        } else {
            toast("No Network Connection!!!");
            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            builder.setCancelable(false);
            builder.setTitle("No Internet");
            builder.setMessage("Internet is required. Please Retry.");

            builder.setNegativeButton("Close", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                 }
            });
            AlertDialog dialog = builder.create(); // calling builder.create after adding buttons
            dialog.show();
        }



    }
    private final MediaControllerCompat.Callback mMediaControllerCallback =
            new MediaControllerCompat.Callback() {
                @Override
                public void onMetadataChanged(MediaMetadataCompat metadata) {
                    super.onMetadataChanged(metadata);
                    if (metadata == null) {
                        return;
                    }

                    adapter.notifyDataSetChanged();
                }

                @Override
                public void onPlaybackStateChanged(@NonNull PlaybackStateCompat state) {
                    super.onPlaybackStateChanged(state);

                 //   checkForUserVisibleErrors(false);
                    adapter.notifyDataSetChanged();
                }
            };
    @Override
    public void onStop() {
        super.onStop();
        MediaBrowserCompat mediaBrowser = mMediaFragmentListener.getMediaBrowser();
        if (mediaBrowser != null && mediaBrowser.isConnected() && mMediaId != null) {
            mediaBrowser.unsubscribe(mMediaId);
        }
        MediaControllerCompat controller = ((FragmentActivity) getActivity())
                .getSupportMediaController();
        if (controller != null) {
            controller.unregisterCallback(mMediaControllerCallback);
        }
        this.getActivity().unregisterReceiver(mConnectivityChangeReceiver);
    }
    @Override
    public void onStart() {
        super.onStart();
        MediaBrowserCompat mediaBrowser = mMediaFragmentListener.getMediaBrowser();


        if (mediaBrowser.isConnected()) {
            onConnected();
        }

        // Registers BroadcastReceiver to track network connection changes.
        this.getActivity().registerReceiver(mConnectivityChangeReceiver,
                new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
    }
    private final BroadcastReceiver mConnectivityChangeReceiver = new BroadcastReceiver() {
        private boolean oldOnline = false;
        @Override
        public void onReceive(Context context, Intent intent) {
            // We don't care about network changes while this fragment is not associated
            // with a media ID (for example, while it is being initialized)
            if (mMediaId != null) {
                boolean isOnline = NetworkHelper.isOnline(context);
                if (isOnline != oldOnline) {
                    oldOnline = isOnline;
                 //   checkForUserVisibleErrors(false);
                    if (isOnline) {
                        adapter.notifyDataSetChanged();
                    }
                }
            }
        }
    };

    public void toast (String text){
        Toast.makeText(getActivity(),text, Toast.LENGTH_SHORT).show();

    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mMediaFragmentListener = (SongFragment.MediaFragmentListener) activity;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
//        int id = item.getItemId();
//
//        //noinspection SimplifiableIfStatement
//        if (id == R.id.action_settings) {
//
//
//
//            android.support.v7.app.AlertDialog.Builder localBuilder = new android.support.v7.app.AlertDialog.Builder(getActivity());
//            localBuilder.setTitle(getString(R.string.action_settings));
//            localBuilder
//                    .setMessage(getString(R.string.disclaimer)).setNegativeButton("Close", new DialogInterface.OnClickListener() {
//                        @Override
//                        public void onClick(
//                                DialogInterface paramAnonymousDialogInterface,
//                                int paramAnonymousInt) {
//                            paramAnonymousDialogInterface.dismiss();
//
//                        }
//                    });
//            localBuilder.show();
//
//        }

        return super.onOptionsItemSelected(item);
    }
    @Override
    public void onCreateOptionsMenu(Menu menu , MenuInflater inflater) {
        // Inflate the menu; this adds items to the action bar if it is present.
        //getMenuInflater().inflate(R.menu.menu_main, menu);
        inflater.inflate(R.menu.main, menu);
        MenuItem searchMenuItem = menu.findItem(R.id.action_search);
        searchMenuItem.setVisible(true);

        mSearchView = (SearchView) searchMenuItem.getActionView();
        mSearchView.setSuggestionsAdapter(mAdapter);

        mSearchView.setOnSuggestionListener(new SearchView.OnSuggestionListener() {
                                                @Override
                                                public boolean onSuggestionSelect(int position) {
                                                    return false;
                                                }
                                                @Override
                                                public boolean onSuggestionClick(int position) {

                                                    if (Final_Suggestions != null && Final_Suggestions.length > 0) {
                                                        mSearchView.setQuery(Final_Suggestions[position], false);
                                                        //processSearchData(Final_Suggestions[position], false);
                                                        search(Final_Suggestions[position]);

                                                    }
                                                    return false;
                                                }

                                            }

        );

        mSearchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                search(s);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                if (!s.matches("")) {
                    // s=URLEncoder.encode(s, "utf-8");

                    if (JSONParser.isNetworkAvailable(getActivity())) {
                        new HttpAsyncTaskSuggestion().execute(String.format(URL_FORMAT_SUGESSTION, Uri.encode(s,"UTF-8")));
                    }
                    SearchText = s;


                }
                return false;
            }
        });

       // return true;
    }
    public String getMediaId() {
        Bundle args = getArguments();
        if (args != null) {
            return args.getString(ARG_MEDIA_ID);
        }
        return null;
    }

    private final MediaBrowserCompat.SubscriptionCallback mSubscriptionCallback =
            new MediaBrowserCompat.SubscriptionCallback() {
                @Override
                public void onChildrenLoaded(@NonNull String parentId,
                                             @NonNull List<MediaBrowserCompat.MediaItem> children) {
                    try {


                        adapter.clear();
                        for (MediaBrowserCompat.MediaItem item : children) {
                            adapter.add(item);
                        }


                        loading.setVisibility(View.INVISIBLE);
                        lv.setVisibility(View.VISIBLE);
                        adapter.notifyDataSetChanged();
                    } catch (Throwable t) {
                        loading.setVisibility(View.INVISIBLE);
                        Toast.makeText(getActivity(), R.string.error_loading_media, Toast.LENGTH_LONG).show();

                    }
                }

                @Override
                public void onError(@NonNull String id) {
                    loading.setVisibility(View.INVISIBLE);
                    Toast.makeText(getActivity(), R.string.error_loading_media, Toast.LENGTH_LONG).show();

                }
            };
    

    private class HttpAsyncTaskSuggestion extends AsyncTask<String, Void, JSONArray> {
        JSONParser FJson = new JSONParser();
        @Override
        protected JSONArray doInBackground(String... urls) {
            return FJson.getJSONFromUrl(urls[0]);
        }
        // onPostExecute displays the results of the AsyncTask.
        @Override
        protected void onPostExecute(JSONArray result) {
            String resultU=result.toString();
            String replace1= "[\""+SearchText+"\",[";
            String replace2= "]]";
            Final_Suggestions = resultU.replace(replace1, "").replace(replace2, "").replace("\"","").replace("[","").split("\\,");
            // Toast.makeText(getBaseContext(), SearchText, Toast.LENGTH_LONG).show();
            final MatrixCursor c = new MatrixCursor(new String[]{ BaseColumns._ID, "cityName" });
            for (int i=0; i<Final_Suggestions.length; i++) {
                if (Final_Suggestions[i].toLowerCase().startsWith(SearchText))
                    Final_Suggestions[i]=Final_Suggestions[i];
                c.addRow(new Object[] {i, Final_Suggestions[i]});
            }
            mAdapter.changeCursor(c);

        }
    }

    private void updateTitle() {
        if (MediaIDHelper.MEDIA_ID_ROOT.equals(mMediaId)) {
            mMediaFragmentListener.setToolbarTitle(null);
            return;
        }

        MediaBrowserCompat mediaBrowser = mMediaFragmentListener.getMediaBrowser();
        mediaBrowser.getItem(mMediaId, new MediaBrowserCompat.ItemCallback() {
            @Override
            public void onItemLoaded(MediaBrowserCompat.MediaItem item) {
                mMediaFragmentListener.setToolbarTitle(
                        item.getDescription().getTitle());
            }
        });
    }
    public void setMediaId(String mediaId) {
        Bundle args = new Bundle(1);
        args.putString(SongFragment.ARG_MEDIA_ID, mediaId);
        setArguments(args);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mMediaFragmentListener = null;
    }
    public static class BrowseAdapter extends ArrayAdapter<MediaBrowserCompat.MediaItem> {

        public BrowseAdapter(Activity context) {
            super(context, R.layout.media_list_item, new ArrayList<MediaBrowserCompat.MediaItem>());
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            MediaBrowserCompat.MediaItem item = getItem(position);
            return MediaItemViewHolder.setupListView((Activity) getContext(), convertView, parent,
                    item);
        }


    }

    public interface MediaFragmentListener extends MediaBrowserProvider {
        void onMediaItemSelected(MediaBrowserCompat.MediaItem item);
        void setToolbarTitle(CharSequence title);
    }
    
public void onConnected() {
    if (isDetached()) {
        return;
    }
    mMediaId = getMediaId();
    if (mMediaId == null) {
        mMediaId = constants.SEARCH_ID;
    }
    updateTitle();


    mMediaFragmentListener.getMediaBrowser().unsubscribe(mMediaId);




    // Add MediaController callback so we can redraw the list when metadata changes:
    MediaControllerCompat controller = ((FragmentActivity) getActivity())
            .getSupportMediaController();
    if (controller != null) {
        controller.registerCallback(mMediaControllerCallback);
    }
}






}