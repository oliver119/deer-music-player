package com.deer.musicplayer.api.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by User on 8/16/2017.
 */

public class Collection_List_Song {
    @SerializedName("item")
    @Expose
    private List<Collection_Song> item = null;

    public List<Collection_Song> getItem() {
        return item;
    }

    public void setItem(List<Collection_Song> item) {
        this.item = item;
    }

}
