package com.deer.musicplayer.api;

import android.os.Handler;
import android.os.Message;

import com.deer.musicplayer.api.model.Collection_List_Song;
import com.deer.musicplayer.api.model.ZingSong;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class RestfulController {


    public static void get_ZingSong(final Handler handler,String id) {
        APIService service = RestfulClient.getClient();
        String data = "{\"id\":\""+id+"\"}";
        Call<ZingSong> call = service.get_ZingSong(data);
        call.enqueue(new Callback<ZingSong>() {
            @Override
            public void onResponse(Call<ZingSong> call, Response<ZingSong> response) {

                Message message = new Message();
                if (response.isSuccessful()) {
                    ZingSong infoModel = response.body();
                    message.obj = infoModel;
                }
                handler.sendMessage(message);
            }

            @Override
            public void onFailure(Call<ZingSong> call, Throwable t) {
                Message message = new Message();
                handler.sendMessage(message);
            }
        });
    }

    public static void getSongCollection(final Handler handler, String url) {
        APIService service = RestfulClientCollection.getClient(url);

        Call<Collection_List_Song> call = service.get_SongCollectionID();
        call.enqueue(new Callback<Collection_List_Song>() {
            @Override
            public void onResponse(Call<Collection_List_Song> call, Response<Collection_List_Song> response) {

                Message message = new Message();
                if (response.isSuccessful()) {
                    Collection_List_Song infoModel = response.body();
                    message.obj = infoModel;
                }
                handler.sendMessage(message);
            }

            @Override
            public void onFailure(Call<Collection_List_Song> call, Throwable t) {
                Message message = new Message();
                handler.sendMessage(message);
            }
        });
    }




}
