package com.deer.musicplayer.api.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ZingSong {

    @SerializedName("song_id")
    @Expose
    private Integer songId;
    @SerializedName("song_id_encode")
    @Expose
    private String songIdEncode;
    @SerializedName("video_id_encode")
    @Expose
    private String videoIdEncode;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("artist_id")
    @Expose
    private String artistId;
    @SerializedName("artist")
    @Expose
    private String artist;
    @SerializedName("album_id")
    @Expose
    private Integer albumId;
    @SerializedName("album")
    @Expose
    private String album;
    @SerializedName("composer_id")
    @Expose
    private Integer composerId;
    @SerializedName("composer")
    @Expose
    private String composer;
    @SerializedName("genre_id")
    @Expose
    private String genreId;
    @SerializedName("isrc")
    @Expose
    private String isrc;
    @SerializedName("zaloid")
    @Expose
    private Integer zaloid;
    @SerializedName("username")
    @Expose
    private String username;
    @SerializedName("is_hit")
    @Expose
    private Integer isHit;
    @SerializedName("is_official")
    @Expose
    private Integer isOfficial;
    @SerializedName("duration")
    @Expose
    private Integer duration;
    @SerializedName("download_status")
    @Expose
    private Integer downloadStatus;
    @SerializedName("copyright")
    @Expose
    private String copyright;
    @SerializedName("co_id")
    @Expose
    private Integer coId;
    @SerializedName("ad_status")
    @Expose
    private Integer adStatus;
    @SerializedName("license_status")
    @Expose
    private Integer licenseStatus;
    @SerializedName("lyrics_file")
    @Expose
    private String lyricsFile;
    @SerializedName("vn_only")
    @Expose
    private Boolean vnOnly;
    @SerializedName("total_play")
    @Expose
    private Integer totalPlay;
    @SerializedName("link")
    @Expose
    private String link;
    @SerializedName("source")
    @Expose
    private Source source;
    @SerializedName("link_download")
    @Expose
    private LinkDownload linkDownload;
    @SerializedName("thumbnail")
    @Expose
    private String thumbnail;
    @SerializedName("album_cover")
    @Expose
    private String albumCover;
    @SerializedName("likes")
    @Expose
    private Integer likes;
    @SerializedName("like_this")
    @Expose
    private Boolean likeThis;
    @SerializedName("favourites")
    @Expose
    private Integer favourites;
    @SerializedName("favourite_this")
    @Expose
    private Boolean favouriteThis;
    @SerializedName("comments")
    @Expose
    private Integer comments;
    @SerializedName("genre_name")
    @Expose
    private String genreName;
    @SerializedName("video")
    @Expose
    private Video video;
    @SerializedName("response")
    @Expose
    private Response response;

    public Integer getSongId() {
        return songId;
    }

    public void setSongId(Integer songId) {
        this.songId = songId;
    }

    public String getSongIdEncode() {
        return songIdEncode;
    }

    public void setSongIdEncode(String songIdEncode) {
        this.songIdEncode = songIdEncode;
    }

    public String getVideoIdEncode() {
        return videoIdEncode;
    }

    public void setVideoIdEncode(String videoIdEncode) {
        this.videoIdEncode = videoIdEncode;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getArtistId() {
        return artistId;
    }

    public void setArtistId(String artistId) {
        this.artistId = artistId;
    }

    public String getArtist() {
        return artist;
    }

    public void setArtist(String artist) {
        this.artist = artist;
    }

    public Integer getAlbumId() {
        return albumId;
    }

    public void setAlbumId(Integer albumId) {
        this.albumId = albumId;
    }

    public String getAlbum() {
        return album;
    }

    public void setAlbum(String album) {
        this.album = album;
    }

    public Integer getComposerId() {
        return composerId;
    }

    public void setComposerId(Integer composerId) {
        this.composerId = composerId;
    }

    public String getComposer() {
        return composer;
    }

    public void setComposer(String composer) {
        this.composer = composer;
    }

    public String getGenreId() {
        return genreId;
    }

    public void setGenreId(String genreId) {
        this.genreId = genreId;
    }

    public String getIsrc() {
        return isrc;
    }

    public void setIsrc(String isrc) {
        this.isrc = isrc;
    }

    public Integer getZaloid() {
        return zaloid;
    }

    public void setZaloid(Integer zaloid) {
        this.zaloid = zaloid;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Integer getIsHit() {
        return isHit;
    }

    public void setIsHit(Integer isHit) {
        this.isHit = isHit;
    }

    public Integer getIsOfficial() {
        return isOfficial;
    }

    public void setIsOfficial(Integer isOfficial) {
        this.isOfficial = isOfficial;
    }

    public Integer getDuration() {
        return duration;
    }

    public void setDuration(Integer duration) {
        this.duration = duration;
    }

    public Integer getDownloadStatus() {
        return downloadStatus;
    }

    public void setDownloadStatus(Integer downloadStatus) {
        this.downloadStatus = downloadStatus;
    }

    public String getCopyright() {
        return copyright;
    }

    public void setCopyright(String copyright) {
        this.copyright = copyright;
    }

    public Integer getCoId() {
        return coId;
    }

    public void setCoId(Integer coId) {
        this.coId = coId;
    }

    public Integer getAdStatus() {
        return adStatus;
    }

    public void setAdStatus(Integer adStatus) {
        this.adStatus = adStatus;
    }

    public Integer getLicenseStatus() {
        return licenseStatus;
    }

    public void setLicenseStatus(Integer licenseStatus) {
        this.licenseStatus = licenseStatus;
    }

    public String getLyricsFile() {
        return lyricsFile;
    }

    public void setLyricsFile(String lyricsFile) {
        this.lyricsFile = lyricsFile;
    }

    public Boolean getVnOnly() {
        return vnOnly;
    }

    public void setVnOnly(Boolean vnOnly) {
        this.vnOnly = vnOnly;
    }

    public Integer getTotalPlay() {
        return totalPlay;
    }

    public void setTotalPlay(Integer totalPlay) {
        this.totalPlay = totalPlay;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public Source getSource() {
        return source;
    }

    public void setSource(Source source) {
        this.source = source;
    }

    public LinkDownload getLinkDownload() {
        return linkDownload;
    }

    public void setLinkDownload(LinkDownload linkDownload) {
        this.linkDownload = linkDownload;
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }

    public String getAlbumCover() {
        return albumCover;
    }

    public void setAlbumCover(String albumCover) {
        this.albumCover = albumCover;
    }

    public Integer getLikes() {
        return likes;
    }

    public void setLikes(Integer likes) {
        this.likes = likes;
    }

    public Boolean getLikeThis() {
        return likeThis;
    }

    public void setLikeThis(Boolean likeThis) {
        this.likeThis = likeThis;
    }

    public Integer getFavourites() {
        return favourites;
    }

    public void setFavourites(Integer favourites) {
        this.favourites = favourites;
    }

    public Boolean getFavouriteThis() {
        return favouriteThis;
    }

    public void setFavouriteThis(Boolean favouriteThis) {
        this.favouriteThis = favouriteThis;
    }

    public Integer getComments() {
        return comments;
    }

    public void setComments(Integer comments) {
        this.comments = comments;
    }

    public String getGenreName() {
        return genreName;
    }

    public void setGenreName(String genreName) {
        this.genreName = genreName;
    }

    public Video getVideo() {
        return video;
    }

    public void setVideo(Video video) {
        this.video = video;
    }

    public Response getResponse() {
        return response;
    }

    public void setResponse(Response response) {
        this.response = response;
    }

}