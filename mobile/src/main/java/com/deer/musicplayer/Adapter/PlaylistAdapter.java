package com.deer.musicplayer.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.deer.musicplayer.Enity.Playlist_Item;
import com.deer.musicplayer.R;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by User on 8/10/2017.
 */

public class PlaylistAdapter extends RecyclerView.Adapter<PlaylistAdapter.MyViewHolder> {
    private List<Playlist_Item> PlayList;
    private Context viewmain;
    onClickItem listener;

    public class MyViewHolder extends ViewHolder {
        public TextView title;

        private ImageView image;

        public MyViewHolder(View view) {
            super(view);

            title = (TextView) view.findViewById(R.id.playlist_name);
            image = (ImageView) view.findViewById(R.id.playlist_image);
            itemView.setTag(itemView);

        }


    }


    public PlaylistAdapter(List<Playlist_Item> playList, Context context, onClickItem Listener) {
        this.PlayList = playList;
        this.viewmain = context;
        this.listener = Listener;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_playlist, parent, false);
        final MyViewHolder mViewHolder = new MyViewHolder(itemView);


        itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onClick(v, mViewHolder.getPosition());
            }
        });

        return mViewHolder;
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {
        final Playlist_Item event = PlayList.get(position);
        holder.title.setText(event.getPlaylist_name());
        Picasso.with(viewmain).load(event.getImage_url()).into(holder.image);


    }

    @Override
    public int getItemCount() {
        return PlayList.size();
    }


}
