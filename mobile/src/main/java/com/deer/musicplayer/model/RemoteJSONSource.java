/*
 * Copyright (C) 2014 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.deer.musicplayer.model;

import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.provider.MediaStore;
import android.support.v4.media.MediaMetadataCompat;
import android.util.Log;

import com.deer.musicplayer.RealmDB.RealmController;
import com.deer.musicplayer.api.RestfulController;
import com.deer.musicplayer.api.model.Collection_List_Song;
import com.deer.musicplayer.api.model.Collection_Song;
import com.deer.musicplayer.api.model.ZingSong;
import com.deer.musicplayer.song.unit;
import com.deer.musicplayer.utils.DispatchGroup;
import com.deer.musicplayer.utils.LogHelper;
import com.deer.musicplayer.utils.MediaIDHelper;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.kaopiz.kprogresshud.KProgressHUD;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.Iterator;

/**
 * Utility class to get a list of MusicTrack's based on a server-side JSON
 * configuration.
 */
public class RemoteJSONSource implements MusicProviderSource {

    private Runnable runnable;


    private DispatchGroup group = new DispatchGroup();

    private static final String TAG = LogHelper.makeLogTag(RemoteJSONSource.class);
    public  ArrayList<String> TophitID_list;


    //  public static final ArrayList<MediaMetadataCompat> Tracktophit = new ArrayList<MediaMetadataCompat>();


    public static String SEARCH =
        "";

    public static String CATALOG_URL =
            "https://api.soundcloud.com/tracks?client_id=a3e059563d7fd3372b49b37f00a00bcf&q=\"jay chou\"&limit=200";

    public static final String ZING_URL_TOP_10 = "http://mp3.zing.vn/bang-xep-hang/index.html";
    public static final String ZING_URL_VIETNAM_40 = "http://mp3.zing.vn/bang-xep-hang/bai-hat-Viet-Nam/IWZ9Z08I.html";
    public static final String ZING_URL_KPOP_40 = "http://mp3.zing.vn/bang-xep-hang/bai-hat-Han-Quoc/IWZ9Z0BO.html";
    public static final String ZING_URL_US_40 = "http://mp3.zing.vn/bang-xep-hang/bai-hat-Au-My/IWZ9Z0BW.html";
    public static final String ZING_IMAGE_PATH = "http://image.mp3.zdn.vn/";

    private static final String JSON_MUSIC = "Value";
    private static final String JSON_TITLE = "title";
    private static final String JSON_ALBUM = "genre";
    private static final String JSON_ARTIST = "genre";
    private static final String JSON_GENRE = "genre";
    private static final String JSON_SOURCE = "stream_url";
    private static final String JSON_IMAGE = "artwork_url";
    private static final String JSON_TRACK_NUMBER = "download_count";
    private static final String JSON_TOTAL_TRACK_COUNT = "likes_count";
    private static final String JSON_DURATION = "duration";

    static JSONArray jarray = null;
    static JSONObject Object = null;

    RealmController realmController;


    public  RemoteJSONSource(String url, String search){

        if(search.equals("1")){
            CATALOG_URL= "https://api.soundcloud.com/tracks?client_id=a3e059563d7fd3372b49b37f00a00bcf&q=\"jay chou\"&limit=200";


        }else {
            CATALOG_URL = url;
        }

    }

    @Override
    public Iterator<MediaMetadataCompat> iterator() {

        try {
            int slashPos = CATALOG_URL.lastIndexOf('/');
           // String path = CATALOG_URL.substring(0, slashPos + 1);
            String path = "";
            //JSONObject jsonObj = fetchJSONFromUrl(CATALOG_URL);



            ArrayList<MediaMetadataCompat> tracks = new ArrayList<>();

          //  if (jsonObj != null) {
            //    JSONArray jsonTracks = jsonObj.getJSONArray(JSON_MUSIC);

            JSONArray jsonTracks =fetchJSONFromUrlArray(CATALOG_URL);

                if (jsonTracks != null) {
                    for (int j = 0; j < jsonTracks.length(); j++) {
                        tracks.add(buildFromJSON(jsonTracks.getJSONObject(j), path));
                    }
                }
       //     }
            return tracks.iterator();
        } catch (JSONException e) {
            LogHelper.e(TAG, e, "Could not retrieve music list");
            throw new RuntimeException("Could not retrieve music list", e);
        }
    }

    public static ArrayList<MediaMetadataCompat> getsongCollection(String url) {


        try {

            ArrayList<MediaMetadataCompat> tracks = new ArrayList<>();

            //  if (jsonObj != null) {
            //    JSONArray jsonTracks = jsonObj.getJSONArray(JSON_MUSIC);

            JSONObject jsonOject = fetchJSONFromUrl(url);


            JSONArray jsonTracks = jsonOject.getJSONArray("item");


            if (jsonTracks != null) {
                for (int j = 0; j < jsonTracks.length(); j++) {


                    RestfulController.get_ZingSong(new Handler(Looper.getMainLooper()) {
                        @Override
                        public void handleMessage(Message message) {

                            try {

                                if (message == null || message.obj == null) {

                                } else {

                                    ZingSong Cha = (ZingSong) message.obj;


                                    String source = Cha.getSource().get128();
                                    String title = Cha.getTitle();
                                    String album = Cha.getAlbum();
                                    String artist = Cha.getArtist();
                                    String genre = "Tophit";
                                    String iconUrl = ZING_IMAGE_PATH + Cha.getThumbnail();


                                    int Duration = Cha.getDuration() * 1000;
                                    String id = Cha.getSongId().toString();

                                    TopList event = new TopList();

                                    if (title.isEmpty() || title.equalsIgnoreCase(null) || title.equals("") || title == null) {

                                    } else {



                                        //  realmController.add_song(realm, title, iconUrl, album, artist, genre, Duration, "topten", id, source);

                                        //    track.add(buildFromCursor(title, album, artist, genre, source, iconUrl, 3, 3, Duration, id));

                                        //   event.setToplist(track);

                                    }


                                }
                            } catch (NullPointerException e) {

                            }


                        }


                    }, jsonTracks.getJSONObject(j).get("id").toString());


                }
            }

            return tracks;
        } catch (JSONException e) {
            LogHelper.e(TAG, e, "Could not retrieve music list");
            throw new RuntimeException("Could not retrieve music list", e);
        }


    }
    @Override
    public Iterator<MediaMetadataCompat> Library_iterator(final Context context) {


        ContentResolver musicResolver = context.getContentResolver();
        Uri musicUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
            Cursor musicCursor = musicResolver.query(musicUri, null, null, null, null);

            ArrayList<MediaMetadataCompat> tracks = new ArrayList<>();


        int count = 0;

        if(musicCursor != null)
        {
            count = musicCursor.getCount();

            if(count > 0)
            {
                while(musicCursor.moveToNext())
                {
                    String source = musicCursor.getString(musicCursor.getColumnIndex(MediaStore.Audio.Media.DATA));
                    String title = musicCursor.getString(musicCursor.getColumnIndex(MediaStore.Audio.Media.TITLE));
                    String album = musicCursor.getString(musicCursor.getColumnIndex(MediaStore.Audio.Media.ALBUM));
                    String artist = musicCursor.getString(musicCursor.getColumnIndex(MediaStore.Audio.Media.ARTIST));
                    String genre = "unknow";
                    String iconUrl = getAlbumArtUri(Long.parseLong(musicCursor.getString(musicCursor.getColumnIndex(MediaStore.Audio.Media.ALBUM_ID)))).toString();



                    int Duration = Integer.parseInt(musicCursor.getString(musicCursor.getColumnIndex(MediaStore.Audio.Media.DURATION)));
                    String id = musicCursor.getString(musicCursor.getColumnIndex(MediaStore.Audio.Media._ID));

                    tracks.add(buildFromCursor(title,album,artist,genre,source,iconUrl,3,3,Duration,id));
                }

            }
        }

        musicCursor.close();



            return tracks.iterator();

    }



    @Override
    public void creatingdata(final Context context, final KProgressHUD hud) {


        realmController = new RealmController(context);
        realmController.deleteall();


        runnable = new Runnable() {
            @Override
            public void run() {
                hud.dismiss();

                //Toast.makeText(context, "oK", Toast.LENGTH_SHORT).show();
            }
        };

        group.enter();
        addCollection(context);
        loaddata();

    }


    private void loaddata() {

        TophitID_list= new ArrayList<String>();
        //   group.enter();
        new DownloadTask().execute(ZING_URL_VIETNAM_40, unit.TOPHIT_VID);
        //   group.enter();
        new DownloadTask().execute(ZING_URL_KPOP_40, unit.TOPHIT_KID);
        //   group.enter();
        new DownloadTask().execute(ZING_URL_US_40, unit.TOPHIT_USID);


        //  hud.dismiss();
        //  loadplaylist();

    }

    public static MediaMetadataCompat buildFromJSON(JSONObject json, String basePath) throws JSONException {
        String title = json.getString(JSON_TITLE);
        String album = json.getString(JSON_ALBUM);
        String artist = json.getString(JSON_ARTIST);
       // String genre = json.getString(JSON_GENRE);

        String genre = "unknow";

        String source = json.getString(JSON_SOURCE);

        String iconUrl = json.getString(JSON_IMAGE).replace("large", "t500x500");


        int trackNumber = json.getInt(JSON_TRACK_NUMBER);
        int totalTrackCount = json.getInt(JSON_TOTAL_TRACK_COUNT);
        int duration = json.getInt(JSON_DURATION); // ms

        LogHelper.d(TAG, "Found music track: ", json);

        // Media is stored relative to JSON file
        if (source.startsWith("http")) {
            source = basePath + source+"?client_id=a3e059563d7fd3372b49b37f00a00bcf";
        }
        if (iconUrl.startsWith("http")) {
            iconUrl = basePath + iconUrl;
        }
        // Since we don't have a unique ID in the server, we fake one using the hashcode of
        // the music source. In a real world app, this could come from the server.
        String id = String.valueOf(source.hashCode());

        // Adding the music source to the MediaMetadata (and consequently using it in the
        // mediaSession.setMetadata) is not a good idea for a real world music app, because
        // the session metadata can be accessed by notification listeners. This is done in this
        // sample for convenience only.
        //noinspection ResourceType
        return new MediaMetadataCompat.Builder()
                .putString(MediaMetadataCompat.METADATA_KEY_MEDIA_ID, id)
                .putString(MusicProviderSource.CUSTOM_METADATA_TRACK_SOURCE, source)
                .putString(MediaMetadataCompat.METADATA_KEY_ALBUM, album)
                .putString(MediaMetadataCompat.METADATA_KEY_ARTIST, artist)
                .putLong(MediaMetadataCompat.METADATA_KEY_DURATION, duration)
                .putString(MediaMetadataCompat.METADATA_KEY_GENRE, genre)
                .putString(MediaMetadataCompat.METADATA_KEY_ALBUM_ART_URI, iconUrl)
                .putString(MediaMetadataCompat.METADATA_KEY_TITLE, title)
                .putLong(MediaMetadataCompat.METADATA_KEY_TRACK_NUMBER, trackNumber)
                .putLong(MediaMetadataCompat.METADATA_KEY_NUM_TRACKS, totalTrackCount)
                .build();
    }


    public static MediaMetadataCompat buildFromCursor(String title, String album, String artist, String genre,  String source,  String iconUrl,int trackNumber, int totalTrackCount,int duration,String id  ) {

        return new MediaMetadataCompat.Builder()
                .putString(MediaMetadataCompat.METADATA_KEY_MEDIA_ID, id)
                .putString(MusicProviderSource.CUSTOM_METADATA_TRACK_SOURCE, source)
                .putString(MediaMetadataCompat.METADATA_KEY_ALBUM, album)
                .putString(MediaMetadataCompat.METADATA_KEY_ARTIST, artist)
                .putLong(MediaMetadataCompat.METADATA_KEY_DURATION, duration)
                .putString(MediaMetadataCompat.METADATA_KEY_GENRE, genre)
                .putString(MediaMetadataCompat.METADATA_KEY_ALBUM_ART_URI, iconUrl)
                .putString(MediaMetadataCompat.METADATA_KEY_TITLE, title)
                .putLong(MediaMetadataCompat.METADATA_KEY_TRACK_NUMBER, trackNumber)
                .putLong(MediaMetadataCompat.METADATA_KEY_NUM_TRACKS, totalTrackCount)
                .build();
    }

    /**
     * Download a JSON file from a server, parse the content and return the JSON
     * object.
     *
     * @return result JSONObject containing the parsed representation.
     */
    private static JSONObject fetchJSONFromUrl(String urlString) throws JSONException {
        BufferedReader reader = null;
        try {
            URLConnection urlConnection = new URL(urlString).openConnection();
            reader = new BufferedReader(new InputStreamReader(
                    urlConnection.getInputStream(), "iso-8859-1"));
            StringBuilder sb = new StringBuilder();
            String line;
            while ((line = reader.readLine()) != null) {
                sb.append(line);
            }
            return new JSONObject(sb.toString());

        } catch (JSONException e) {
            throw e;
        } catch (Exception e) {
            LogHelper.e(TAG, "Failed to parse the json for media list", e);
            return null;
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e) {
                    // ignore
                }
            }
        }
    }

    private JSONArray fetchJSONFromUrlArray(String urlString) throws JSONException {
        BufferedReader reader = null;
        try {
          //  URLConnection urlConnection = new URL(urlString).openConnection();
            //urlConnection.connect();
            URL url = new URL(urlString);
            HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.setRequestMethod("GET");
            urlConnection.addRequestProperty("q","chi dân");
         //   urlConnection.setDoOutput(true);
            urlConnection.connect();


            int responseCode = urlConnection.getResponseCode(); //can call this instead of con.connect()
            if (responseCode >= 400 && responseCode <= 499) {
                throw new Exception("Bad authentication status: " + responseCode); //provide a more meaningful exception message
            }
            else {

            }


            reader = new BufferedReader(new InputStreamReader(
                    urlConnection.getInputStream(),"UTF-8"));
            StringBuilder sb = new StringBuilder();
            String line;
            while ((line = reader.readLine()) != null) {
                sb.append(line);
            }
            return new JSONArray(sb.toString());
        } catch (JSONException e) {
            throw e;
        } catch (Exception e) {
            LogHelper.e(TAG, "Failed to parse the json for media list", e);
            return null;
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e) {
                    // ignore
                }
            }
        }
    }

    public static JSONObject getJSONFromUrl(String url) {

        StringBuilder builder = new StringBuilder();
        HttpClient client = new DefaultHttpClient();
        HttpGet httpGet = new HttpGet(url);
        try {
            HttpResponse response = client.execute(httpGet);
            StatusLine statusLine = response.getStatusLine();
            int statusCode = statusLine.getStatusCode();
            if (statusCode == 200) {
                HttpEntity entity = response.getEntity();
                InputStream content = entity.getContent();
                BufferedReader reader = new BufferedReader(new InputStreamReader(content));
                String line;
                while ((line = reader.readLine()) != null) {
                    builder.append(line);
                }
            } else {
                Log.e("==>", "Failed to download file");
            }
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        // Parse String to JSON object
        try {
            Object = new JSONObject(builder.toString());
        } catch (JSONException e) {
            Log.e("JSON Parser", "Error parsing data " + e.toString());
        }

        // return JSON Object
        return Object;

    }
    /////

    public static Uri getAlbumArtUri(long albumId) {
        return ContentUris.withAppendedId(Uri.parse("content://media/external/audio/albumart"), albumId);
    }

    private class DownloadTask extends AsyncTask<String, Void, ArrayList<String>> {

        private static final String TAG = "DownloadTask";
        private String url = "";

        @Override
        protected ArrayList<String> doInBackground(String... strings) {
            Document document = null;
            ArrayList<String> listsong = new ArrayList<>();

            url = strings[1].toString();
            if (strings[1].equals("1")) {
                try {
                    document = Jsoup.connect(strings[0]).get();
                    if (document != null) {
                        //Lấy  html có thẻ như sau: div#latest-news > div.row > div.col-md-6 hoặc chỉ cần dùng  div.col-md-6
                        // Elements sub = document.select("div.bordered non-bg-rank > li > h3.song-name");
                        Elements sub = document.select("h3[class=song-name]");
                        for (Element element : sub) {

                            Element song = element.getElementsByTag("a").first();
                            //Parse to model

                            if (song != null) {
                                String link = song.attr("href");

                                listsong.add(getID_ZING(link));

                            }


                        }
                    }

                } catch (IOException e) {
                    e.printStackTrace();
                }
            } else if (strings[1].equals(unit.TOPHIT_KID)) {
                try {
                    document = Jsoup.connect(strings[0]).get();
                    if (document != null) {
                        //Lấy  html có thẻ như sau: div#latest-news > div.row > div.col-md-6 hoặc chỉ cần dùng  div.col-md-6
                        // Elements sub = document.select("div.bordered non-bg-rank > li > h3.song-name");
                        Elements sub = document.select("li[data-type=song]");
                        for (Element element : sub) {

                            // Element song = element.getElementsByTag("a").first();

                            String link = element.attr("data-id").toString();
                            //Parse to model

                            if (link != null) {
                                group.enter();
                                listsong.add(link);
                            }


                        }

                        //      getlistdata(listsong, strings[1]);


                    }

                } catch (IOException e) {
                    e.printStackTrace();
                }
            } else if (strings[1].equals(unit.TOPHIT_VID)) {
                try {
                    document = Jsoup.connect(strings[0]).get();
                    if (document != null) {
                        //Lấy  html có thẻ như sau: div#latest-news > div.row > div.col-md-6 hoặc chỉ cần dùng  div.col-md-6
                        // Elements sub = document.select("div.bordered non-bg-rank > li > h3.song-name");
                        Elements sub = document.select("li[data-type=song]");
                        for (Element element : sub) {

                            // Element song = element.getElementsByTag("a").first();

                            String link = element.attr("data-id").toString();
                            //Parse to model

                            if (link != null) {
                                group.enter();
                                listsong.add(link);
                            }


                        }

                        //    getlistdata(listsong, strings[1]);


                    }

                } catch (IOException e) {
                    e.printStackTrace();
                }
            } else if (strings[1].equals(unit.TOPHIT_USID)) {
                try {
                    document = Jsoup.connect(strings[0]).get();
                    if (document != null) {
                        //Lấy  html có thẻ như sau: div#latest-news > div.row > div.col-md-6 hoặc chỉ cần dùng  div.col-md-6
                        // Elements sub = document.select("div.bordered non-bg-rank > li > h3.song-name");
                        Elements sub = document.select("li[data-type=song]");
                        for (Element element : sub) {

                            // Element song = element.getElementsByTag("a").first();

                            String link = element.attr("data-id").toString();
                            //Parse to model

                            if (link != null) {
                                group.enter();
                                listsong.add(link);
                            }


                        }

                        //      getlistdata(listsong, strings[1]);
////

                    }

                } catch (IOException e) {
                    e.printStackTrace();
                }

            } else {
                try {
                    document = Jsoup.connect(strings[0]).get();
                    if (document != null) {
                        //Lấy  html có thẻ như sau: div#latest-news > div.row > div.col-md-6 hoặc chỉ cần dùng  div.col-md-6
                        // Elements sub = document.select("div.bordered non-bg-rank > li > h3.song-name");
                        Elements sub = document.select("li[data-type=song]");
                        for (Element element : sub) {

                            // Element song = element.getElementsByTag("a").first();

                            String link = element.attr("data-id").toString();
                            //Parse to model

                            if (link != null) {

                                listsong.add(link);
                            }


                        }
                    }

                } catch (IOException e) {
                    e.printStackTrace();
                }

            }
            return listsong;
        }

        @Override
        protected void onPostExecute(ArrayList<String> articles) {
            super.onPostExecute(articles);


            getlistdata(articles, url);


            TophitID_list = articles;
            group.notify(runnable);
            group.leave();


        }
    }

    private void getlistdata(ArrayList<String> list, final String type) {
        //    final ArrayList<MediaMetadataCompat> track = new ArrayList<MediaMetadataCompat>();
        for (int i = 0; i < list.size(); i++) {

            RestfulController.get_ZingSong(new Handler(Looper.getMainLooper()) {
                @Override
                public void handleMessage(Message message) {
                    group.enter();
                    if (type.equals(unit.TOPHIT_VID)) {
                        try {

                            if (message == null || message.obj == null) {

                            } else {

                                ZingSong Cha = (ZingSong) message.obj;


                                String source = Cha.getSource().get128();
                                String title = Cha.getTitle();
                                String album = Cha.getAlbum();
                                String artist = Cha.getArtist();
                                String genre = "Tophit";
                                String iconUrl = ZING_IMAGE_PATH + Cha.getThumbnail();


                                int Duration = Cha.getDuration() * 1000;
                                String id = Cha.getSongId().toString();

                                // TopList event = new TopList();

                                if (title.isEmpty() || title.equalsIgnoreCase(null) || title.equals("") || title == null) {

                                } else {




                                    realmController.add_song(title, iconUrl, album, artist, genre, Duration, unit.TOPHIT_VID, id, source);

                                }


                            }
                            group.leave();
                        } catch (NullPointerException e) {
                            group.leave();
                        }

                    } else if (type.equals(unit.TOPHIT_KID)) {

                        try {

                            if (message == null || message.obj == null) {

                            } else {

                                ZingSong Cha = (ZingSong) message.obj;


                                String source = Cha.getSource().get128();
                                String title = Cha.getTitle();
                                String album = Cha.getAlbum();
                                String artist = Cha.getArtist();
                                String genre = "Tophitkpop";
                                String iconUrl = ZING_IMAGE_PATH + Cha.getThumbnail();


                                int Duration = Cha.getDuration() * 1000;
                                String id = Cha.getSongId().toString();

                                //     TopList event = new TopList();

                                if (title.isEmpty() || title.equalsIgnoreCase(null) || title.equals("") || title == null) {

                                } else {




                                    realmController.add_song(title, iconUrl, album, artist, genre, Duration, unit.TOPHIT_KID, id, source);




                                }



                            }
                            group.leave();
                        } catch (NullPointerException e) {
                            group.leave();
                        }

                    } else if (type.equals(unit.TOPHIT_USID)) {

                        try {

                            if (message == null || message.obj == null) {

                            } else {

                                ZingSong Cha = (ZingSong) message.obj;


                                String source = Cha.getSource().get128();
                                String title = Cha.getTitle();
                                String album = Cha.getAlbum();
                                String artist = Cha.getArtist();
                                String genre = "Tophitus";
                                String iconUrl = ZING_IMAGE_PATH + Cha.getThumbnail();


                                int Duration = Cha.getDuration() * 1000;
                                String id = Cha.getSongId().toString();

                                //   TopList event = new TopList();

                                if (title.isEmpty() || title.equalsIgnoreCase(null) || title.equals("") || title == null) {

                                } else {


                                    realmController.add_song(title, iconUrl, album, artist, genre, Duration, unit.TOPHIT_USID, id, source);




                                }



        }
                            group.leave();
                        } catch (NullPointerException e) {
                            group.leave();
                        }

                    } else {
                        try {

                            if (message == null || message.obj == null) {

                            } else {

                                ZingSong Cha = (ZingSong) message.obj;


                                String source = Cha.getSource().get128();
                                String title = Cha.getTitle();
                                String album = Cha.getAlbum();
                                String artist = Cha.getArtist();
                                String genre = "Tophit";
                                String iconUrl = ZING_IMAGE_PATH + Cha.getThumbnail();


                                int Duration = Cha.getDuration() * 1000;
                                String id = Cha.getSongId().toString();

                                TopList event = new TopList();

                                if (title.isEmpty() || title.equalsIgnoreCase(null) || title.equals("") || title == null) {

                                } else {


                                    realmController.add_song(title, iconUrl, album, artist, genre, Duration, "topten", id, source);




                                }


                            }
                        } catch (NullPointerException e) {

                        }

                    }


                }


            }, list.get(i));


        }

    }
    private String getID_ZING(String url){
        String id_zing = "";

        int start = url.lastIndexOf(".html");
        id_zing=  url.substring(start-8,start);

        return id_zing;
    }

    private void addCollection(final Context context) {


        DatabaseReference playlist = FirebaseDatabase.getInstance().getReference().child("Collection");

        playlist.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {


                for (DataSnapshot collection : dataSnapshot.getChildren()) {
                    String genre = collection.getKey();
                    String name = collection.child("name").getValue().toString();
                    String icon = collection.child("image").getValue().toString();

                    RealmController realmController = new RealmController(context);
                    group.enter();
                    addCollectCon(genre, realmController);
                    group.enter();
                    realmController.add_collection(name, genre, icon, MediaIDHelper.COLLECTION_MENU);
                }
                group.leave();


            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                group.leave();
            }
        });


    }

    private void addCollectCon(final String colectID, final RealmController realmController) {

        DatabaseReference colect = FirebaseDatabase.getInstance().getReference().child("Collection").child(colectID).child("content");

        colect.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {



                for (DataSnapshot collection : dataSnapshot.getChildren()) {

                    String genre = collection.getKey();
                    String name = collection.child("name").getValue().toString();
                    String icon = collection.child("image").getValue().toString();


                    realmController.add_collection(name, genre, icon, MediaIDHelper.COLLECTION_MENU_CON);
                    group.enter();
                    addCollectTopic(colectID, realmController, genre);

                }
                group.leave();



            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                group.leave();
            }
        });
    }

    private void addCollectTopic(String colectID, final RealmController realmController, String colectMenu) {

        DatabaseReference colect = FirebaseDatabase.getInstance().getReference().child("Collection").child(colectID).child("content").child(colectMenu).child("content");

        colect.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                for (DataSnapshot collection : dataSnapshot.getChildren()) {
                    group.enter();
                    final String genre = collection.getKey();
                    final String name = collection.child("name").getValue().toString();
                    String icon = collection.child("image").getValue().toString();
                    String source = collection.child("source").getValue().toString();


                    realmController.add_collection(name, genre, icon, MediaIDHelper.COLLECTION_MENU_TOPIC);


                    RestfulController.getSongCollection(new Handler(Looper.getMainLooper()) {
                        @Override
                        public void handleMessage(Message message) {

                            try {

                                if (message == null || message.obj == null) {

                                } else {

                                    Collection_List_Song Cha = (Collection_List_Song) message.obj;

                                    for (int i = 0; i < Cha.getItem().size(); i++) {


                                        final Collection_Song event = Cha.getItem().get(i);


                                        RestfulController.get_ZingSong(new Handler(Looper.getMainLooper()) {
                                            @Override
                                            public void handleMessage(Message message) {

                                                try {

                                                    if (message == null || message.obj == null) {

                                                    } else {

                                                        ZingSong Cha = (ZingSong) message.obj;


                                                        String source = Cha.getSource().get128();
                                                        String title = Cha.getTitle();
                                                        String album = Cha.getAlbum();
                                                        String artist = Cha.getArtist();
                                                        String genre = name;
                                                        String iconUrl = ZING_IMAGE_PATH + Cha.getThumbnail();


                                                        int Duration = Cha.getDuration() * 1000;
                                                        String id = Cha.getSongId().toString();

                                                        // TopList event = new TopList();

                                                        if (title.isEmpty() || title.equalsIgnoreCase(null) || title.equals("") || title == null) {


                                                        } else {


                                                            realmController.add_song(title, iconUrl, album, artist, genre, Duration, name, id, source);

                                                        }


                                                    }
                                                    group.leave();
                                                } catch (NullPointerException e) {
                                                    group.leave();
                                                }

                                            }


                                        }, event.getId());


                                    }
                                }


                            } catch (NullPointerException e) {
//// null poing
                                group.leave();

                            }


                        }


                    }, source);

                    group.leave();
                }

            }


            @Override
            public void onCancelled(DatabaseError databaseError) {
                group.leave();
            }
        });
    }

    private void getSongCollection(String url) {


    }
}
