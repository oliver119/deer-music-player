/*
 * Copyright (C) 2014 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.deer.musicplayer.model;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.media.MediaBrowserCompat;
import android.support.v4.media.MediaDescriptionCompat;
import android.support.v4.media.MediaMetadataCompat;

import com.deer.musicplayer.R;
import com.deer.musicplayer.RealmDB.RealmController;
import com.deer.musicplayer.song.constants;
import com.deer.musicplayer.utils.LogHelper;
import com.deer.musicplayer.utils.MediaIDHelper;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

import static com.deer.musicplayer.utils.MediaIDHelper.COLLECTION_;
import static com.deer.musicplayer.utils.MediaIDHelper.COLLECTION_MENU;
import static com.deer.musicplayer.utils.MediaIDHelper.COLLECTION_MENU_CON;
import static com.deer.musicplayer.utils.MediaIDHelper.COLLECTION_MENU_TOPIC;
import static com.deer.musicplayer.utils.MediaIDHelper.MEDIA_ID_MUSICS_BY_GENRE;
import static com.deer.musicplayer.utils.MediaIDHelper.MEDIA_ID_MUSICS_BY_PLAYLIST_NAME;
import static com.deer.musicplayer.utils.MediaIDHelper.MEDIA_ID_ROOT;
import static com.deer.musicplayer.utils.MediaIDHelper.createMediaID;

/**
 * Simple data provider for music tracks. The actual metadata source is delegated to a
 * MusicProviderSource defined by a constructor argument of this class.
 */
public class MusicProvider implements constants {

    private static final String TAG = LogHelper.makeLogTag(MusicProvider.class);
    public static final String TOPHIT_VID = "TOPHIT_VPOP";
    public static final String TOPHIT_KID = "TOPHIT_KPOP";
    public static final String TOPHIT_USID = "TOPHIT_USPOP";
    public static final String PLAYLIST_NAME = "PLAYLIST_NAME";
    public static final String PLAYLIST_ITEM = "PLAYLIST_ITEM";

    private Map<String, Integer> NumberSong;



    private MusicProviderSource mSource;

    // Categorized caches for music track data:
    private ConcurrentMap<String, List<MediaMetadataCompat>> mMusicListByGenre;
    private ConcurrentMap<String, MutableMediaMetadata> mMusicListById;


    private Set<String> mFavoriteTracks;



    enum State {
        NON_INITIALIZED, INITIALIZING, INITIALIZED
    }

    private volatile State mCurrentState = State.NON_INITIALIZED;

    public interface Callback {
        void onMusicCatalogReady(boolean success);
    }

    public MusicProvider(String url,String search) {
        this(new RemoteJSONSource(url,search));
    }
    public MusicProvider(MusicProviderSource source) {
        mSource = source;
        mMusicListByGenre = new ConcurrentHashMap<>();
        mMusicListById = new ConcurrentHashMap<>();
        mFavoriteTracks = Collections.newSetFromMap(new ConcurrentHashMap<String, Boolean>());
    }

    /**
     * Get an iterator over the list of genres
     *
     * @return genres
     */
    public Iterable<String> getGenres() {
        if (mCurrentState != State.INITIALIZED) {
            return Collections.emptyList();
        }
        return mMusicListByGenre.keySet();
    }

    /**
     * Get an iterator over a shuffled collection of all songs
     */
    public Iterable<MediaMetadataCompat> getShuffledMusic() {
        if (mCurrentState != State.INITIALIZED) {
            return Collections.emptyList();
        }
        List<MediaMetadataCompat> shuffled = new ArrayList<>(mMusicListById.size());
        for (MutableMediaMetadata mutableMetadata: mMusicListById.values()) {
            shuffled.add(mutableMetadata.metadata);
        }
        Collections.shuffle(shuffled);
        return shuffled;
    }

    /**
     * Get music tracks of the given genre
     *
     */
    public Iterable<MediaMetadataCompat> getMusicsByGenre(String genre) {
        if (mCurrentState != State.INITIALIZED || !mMusicListByGenre.containsKey(genre)) {
            return Collections.emptyList();
        }
        return mMusicListByGenre.get(genre);
    }

    /**
     * Very basic implementation of a search that filter music tracks with title containing
     * the given query.
     *
     */
    public Iterable<MediaMetadataCompat> searchMusicBySongTitle(String query) {
        return searchMusic(MediaMetadataCompat.METADATA_KEY_TITLE, query);
    }

    /**
     * Very basic implementation of a search that filter music tracks with album containing
     * the given query.
     *
     */
    public Iterable<MediaMetadataCompat> searchMusicByAlbum(String query) {
        return searchMusic(MediaMetadataCompat.METADATA_KEY_ALBUM, query);
    }

    /**
     * Very basic implementation of a search that filter music tracks with artist containing
     * the given query.
     *
     */
    public Iterable<MediaMetadataCompat> searchMusicByArtist(String query) {
        return searchMusic(MediaMetadataCompat.METADATA_KEY_ARTIST, query);
    }

    Iterable<MediaMetadataCompat> searchMusic(String metadataField, String query) {
        if (mCurrentState != State.INITIALIZED) {
            return Collections.emptyList();
        }
        ArrayList<MediaMetadataCompat> result = new ArrayList<>();
        query = query.toLowerCase(Locale.US);
        for (MutableMediaMetadata track : mMusicListById.values()) {
            if (track.metadata.getString(metadataField).toLowerCase(Locale.US)
                .contains(query)) {
                result.add(track.metadata);
            }
        }
        return result;
    }


    /**
     * Return the MediaMetadataCompat for the given musicID.
     *
     * @param musicId The unique, non-hierarchical music ID.
     */
    public MediaMetadataCompat getMusic(String musicId) {
        return mMusicListById.containsKey(musicId) ? mMusicListById.get(musicId).metadata : null;
    }

    public synchronized void updateMusicArt(String musicId, Bitmap albumArt, Bitmap icon) {
        MediaMetadataCompat metadata = getMusic(musicId);
        metadata = new MediaMetadataCompat.Builder(metadata)

                // set high resolution bitmap in METADATA_KEY_ALBUM_ART. This is used, for
                // example, on the lockscreen background when the media session is active.
                .putBitmap(MediaMetadataCompat.METADATA_KEY_ALBUM_ART, albumArt)

                // set small version of the album art in the DISPLAY_ICON. This is used on
                // the MediaDescription and thus it should be small to be serialized if
                // necessary
                .putBitmap(MediaMetadataCompat.METADATA_KEY_DISPLAY_ICON, icon)

                .build();

        MutableMediaMetadata mutableMetadata = mMusicListById.get(musicId);
        if (mutableMetadata == null) {
            throw new IllegalStateException("Unexpected error: Inconsistent data structures in " +
                    "MusicProvider");
        }

        mutableMetadata.metadata = metadata;
    }

    public void setFavorite(String musicId, boolean favorite) {
        if (favorite) {
            mFavoriteTracks.add(musicId);
        } else {
            mFavoriteTracks.remove(musicId);
        }
    }

    public boolean isInitialized() {
        return mCurrentState == State.INITIALIZED;
    }

    public boolean isFavorite(String musicId) {
        return mFavoriteTracks.contains(musicId);
    }

    /**
     * Get the list of music tracks from a server and caches the track information
     * for future reference, keying tracks by musicId and grouping by genre.
     */
    public void retrieveMediaAsync(final Callback callback, String search, final Context context) {
        if (search.equals("1")){
            LogHelper.d(TAG, "retrieveMediaAsync called");
            if (mCurrentState == State.INITIALIZED) {
                if (callback != null) {
                    // Nothing to do, execute callback immediately
                    callback.onMusicCatalogReady(true);
                }
                return;
            }

            // Asynchronously load the music catalog in a separate thread
            new AsyncTask<Void, Void, State>() {
                @Override
                protected State doInBackground(Void... params) {
                    retrieveMedia_Lbrary(context);
                    return mCurrentState;
                }

                @Override
                protected void onPostExecute(State current) {
                    if (callback != null) {
                        callback.onMusicCatalogReady(current == State.INITIALIZED);
                    }
                }
            }.execute();
        } else if (search.equals(TOPHIT_VID)) {
            LogHelper.d(TAG, "retrieveMediaAsync called");
            if (mCurrentState == State.INITIALIZED) {
                if (callback != null) {
                    // Nothing to do, execute callback immediately
                    callback.onMusicCatalogReady(true);
                }
                return;
            }

            // Asynchronously load the music catalog in a separate thread
            new AsyncTask<Void, Void, State>() {
                @Override
                protected State doInBackground(Void... params) {
                    retrieveMedia_Top(context);
                    return mCurrentState;
                }

                @Override
                protected void onPostExecute(State current) {
                    if (callback != null) {
                        callback.onMusicCatalogReady(current == State.INITIALIZED);
                    }
                }
            }.execute();
        } else if (search.equals(TOPHIT_KID)) {
            LogHelper.d(TAG, "retrieveMediaAsync called");
            if (mCurrentState == State.INITIALIZED) {
                if (callback != null) {
                    // Nothing to do, execute callback immediately
                    callback.onMusicCatalogReady(true);
                }
                return;
            }

            // Asynchronously load the music catalog in a separate thread
            new AsyncTask<Void, Void, State>() {
                @Override
                protected State doInBackground(Void... params) {
                    retrieveMedia_Top_k(context);
                    return mCurrentState;
                }

                @Override
                protected void onPostExecute(State current) {
                    if (callback != null) {
                        callback.onMusicCatalogReady(current == State.INITIALIZED);
                    }
                }
            }.execute();
        } else if (search.equals(TOPHIT_USID)) {
            LogHelper.d(TAG, "retrieveMediaAsync called");
            if (mCurrentState == State.INITIALIZED) {
                if (callback != null) {
                    // Nothing to do, execute callback immediately
                    callback.onMusicCatalogReady(true);
                }
                return;
            }

            // Asynchronously load the music catalog in a separate thread
            new AsyncTask<Void, Void, State>() {
                @Override
                protected State doInBackground(Void... params) {
                    retrieveMedia_Top_us(context);
                    return mCurrentState;
                }

                @Override
                protected void onPostExecute(State current) {
                    if (callback != null) {
                        callback.onMusicCatalogReady(current == State.INITIALIZED);
                    }
                }
            }.execute();
        } else if (search.equals(PLAYLIST_NAME)) {
            LogHelper.d(TAG, "retrieveMediaAsync called");
            if (mCurrentState == State.INITIALIZED) {
                if (callback != null) {
                    // Nothing to do, execute callback immediately
                    callback.onMusicCatalogReady(true);
                }
                return;
            }

            // Asynchronously load the music catalog in a separate thread
//            new AsyncTask<Void, Void, State>() {
//                @Override
//                protected State doInBackground(Void... params) {
//                    retrieveMedia_Playlist(context);
//                    return mCurrentState;
//                }
//
//                @Override
//                protected void onPostExecute(State current) {
//                    if (callback != null) {
//                        callback.onMusicCatalogReady(current == State.INITIALIZED);
//                    }
//                }
//            }.execute();


            loadplaylist(context, callback);

        } else if (search.equals(COLLECTION_)) {
            LogHelper.d(TAG, "retrieveMediaAsync called");
            if (mCurrentState == State.INITIALIZED) {
                if (callback != null) {
                    // Nothing to do, execute callback immediately
                    callback.onMusicCatalogReady(true);
                }
                return;
            }


            loadCollectionMenu(context, callback);

        }


        else {


            LogHelper.d(TAG, "retrieveMediaAsync called");
            if (mCurrentState == State.INITIALIZED) {
                if (callback != null) {
                    // Nothing to do, execute callback immediately
                    callback.onMusicCatalogReady(true);
                }
                return;
            }

            // Asynchronously load the music catalog in a separate thread
            new AsyncTask<Void, Void, State>() {
                @Override
                protected State doInBackground(Void... params) {
                    retrieveMedia();
                    return mCurrentState;
                }

                @Override
                protected void onPostExecute(State current) {
                    if (callback != null) {
                        callback.onMusicCatalogReady(current == State.INITIALIZED);
                    }
                }
            }.execute();

        }
    }

    private synchronized void buildListsByGenre() {
        ConcurrentMap<String, List<MediaMetadataCompat>> newMusicListByGenre = new ConcurrentHashMap<>();

        for (MutableMediaMetadata m : mMusicListById.values()) {
            String genre = m.metadata.getString(MediaMetadataCompat.METADATA_KEY_GENRE);
            List<MediaMetadataCompat> list = newMusicListByGenre.get(genre);
            if (list == null) {
                list = new ArrayList<>();
                newMusicListByGenre.put(genre, list);
            }
            list.add(m.metadata);
        }
        mMusicListByGenre = newMusicListByGenre;
    }

    public void retrieveMediaAsyncCollection(final Callback callback, String search, final Context context, String colecID) {

        if (search.equals(COLLECTION_MENU)) {
            LogHelper.d(TAG, "retrieveMediaAsync called");
            if (mCurrentState == State.INITIALIZED) {
                if (callback != null) {
                    // Nothing to do, execute callback immediately
                    callback.onMusicCatalogReady(true);
                }
                return;
            }


            loadCollectionMenuCon(context, callback, colecID);

        } else if (search.equals(COLLECTION_MENU_CON)) {
            LogHelper.d(TAG, "retrieveMediaAsync called");
            if (mCurrentState == State.INITIALIZED) {
                if (callback != null) {
                    // Nothing to do, execute callback immediately
                    callback.onMusicCatalogReady(true);
                }
                return;
            }


            loadCollectionMenuTopic(context, callback, colecID);

        } else if (search.equals(COLLECTION_MENU_TOPIC)) {
            LogHelper.d(TAG, "retrieveMediaAsync called");
            if (mCurrentState == State.INITIALIZED) {
                if (callback != null) {
                    // Nothing to do, execute callback immediately
                    callback.onMusicCatalogReady(true);
                }
                return;
            }


            loadSongCollection(context, callback, colecID);

        } else if (search.equals(COLLECTION_MENU_CON)) {
            LogHelper.d(TAG, "retrieveMediaAsync called");
            if (mCurrentState == State.INITIALIZED) {
                if (callback != null) {
                    // Nothing to do, execute callback immediately
                    callback.onMusicCatalogReady(true);
                }
                return;
            }


            loadSongCollection(context, callback, colecID);

        }

    }

    private synchronized void retrieveMedia() {
        try {
            if (mCurrentState == State.NON_INITIALIZED) {
                mCurrentState = State.INITIALIZING;

                Iterator<MediaMetadataCompat> tracks = mSource.iterator();
                while (tracks.hasNext()) {
                    MediaMetadataCompat item = tracks.next();
                    String musicId = item.getString(MediaMetadataCompat.METADATA_KEY_MEDIA_ID);
                    mMusicListById.put(musicId, new MutableMediaMetadata(musicId, item));
                }
                buildListsByGenre();
                mCurrentState = State.INITIALIZED;
            }
        } finally {
            if (mCurrentState != State.INITIALIZED) {
                // Something bad happened, so we reset state to NON_INITIALIZED to allow
                // retries (eg if the network connection is temporary unavailable)
                mCurrentState = State.NON_INITIALIZED;
            }
        }
    }
    private synchronized void retrieveMedia_Lbrary(Context context) {
        try {
            if (mCurrentState == State.NON_INITIALIZED) {
                mCurrentState = State.INITIALIZING;

                Iterator<MediaMetadataCompat> tracks = mSource.Library_iterator(context);
                while (tracks.hasNext()) {
                    MediaMetadataCompat item = tracks.next();
                    String musicId = item.getString(MediaMetadataCompat.METADATA_KEY_MEDIA_ID);
                    mMusicListById.put(musicId, new MutableMediaMetadata(musicId, item));
                }
                buildListsByGenre();
                mCurrentState = State.INITIALIZED;
            }
        } finally {
            if (mCurrentState != State.INITIALIZED) {
                // Something bad happened, so we reset state to NON_INITIALIZED to allow
                // retries (eg if the network connection is temporary unavailable)
                mCurrentState = State.NON_INITIALIZED;
            }
        }
    }
    private synchronized void retrieveMedia_Top(Context context) {

        RealmController realmController = new RealmController(context);
        try {
            if (mCurrentState == State.NON_INITIALIZED) {
                mCurrentState = State.INITIALIZING;

                Iterator<MediaMetadataCompat> tracks = realmController.Tophit_VPop().iterator();
                while (tracks.hasNext()) {
                    MediaMetadataCompat item = tracks.next();
                    String musicId = item.getString(MediaMetadataCompat.METADATA_KEY_MEDIA_ID);
                    mMusicListById.put(musicId, new MutableMediaMetadata(musicId, item));
                }
                buildListsByGenre();
                mCurrentState = State.INITIALIZED;
            }
        } finally {
            if (mCurrentState != State.INITIALIZED) {
                // Something bad happened, so we reset state to NON_INITIALIZED to allow
                // retries (eg if the network connection is temporary unavailable)
                mCurrentState = State.NON_INITIALIZED;
            }
        }
    }

    private void loadplaylist(final Context context, final Callback callback) {
        final ArrayList<MediaMetadataCompat> tracksplaylist = new ArrayList<>();
        NumberSong = new HashMap<>();

        DatabaseReference playlist = FirebaseDatabase.getInstance().getReference().child("User");

        playlist.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                DataSnapshot cha = dataSnapshot.child(FirebaseAuth.getInstance().getCurrentUser().getUid()).child("playlist");

                for (DataSnapshot playlist : cha.getChildren()) {
                    String genre = playlist.getKey();
                    int numSong = 0;
                    for (DataSnapshot song : playlist.child("song").getChildren()) {
                        numSong++;

                        String id = song.getKey();
                        //DataSnapshot meta = song.child(song.getKey());
                        //  song.child("source_song").getValue().toString();

                        String source = song.child("source_song").getValue().toString();
                        String title = song.child("name_song").getValue().toString();
                        String album = song.child("allbum_song").getValue().toString();
                        String artist = song.child("artist_song").getValue().toString();

                        String iconUrl = song.child("icon_song").getValue().toString();


                        int Duration = Integer.valueOf(song.child("duration_song").getValue().toString());

                        tracksplaylist.add(RemoteJSONSource.buildFromCursor(title, album, artist, genre, source, iconUrl, 3, 3, Duration, id));
                    }
                    NumberSong.put(genre, numSong);

                }

                retrieveMedia_Playlist(tracksplaylist, context);

                if (callback != null) {
                    callback.onMusicCatalogReady(mCurrentState == State.INITIALIZED);
                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

//    private void loadCollection(final Context context, final Callback callback) {
//        final ArrayList<MediaMetadataCompat> tracksplaylist = new ArrayList<>();
//        NumberSong = new HashMap<>();
//
//        DatabaseReference playlist = FirebaseDatabase.getInstance().getReference().child("Collection");
//
//        playlist.addListenerForSingleValueEvent(new ValueEventListener() {
//            @Override
//            public void onDataChange(DataSnapshot dataSnapshot) {
//
//
//                for (DataSnapshot collection : dataSnapshot.getChildren()) {
//                    String genre = collection.getKey();
//                    String name = collection.child("name").getValue().toString();
//                    String icon = collection.child("image").getValue().toString();
//
//
//                    tracksplaylist.add(RemoteJSONSource.buildFromCursor(name, "unknow", "unknow", genre, "unknow", icon, 3, 3, 2, genre));
//
//                }
//
//                retrieveMedia_Collection(tracksplaylist, context);
//
//                if (callback != null) {
//                    callback.onMusicCatalogReady(mCurrentState == State.INITIALIZED);
//                }
//
//            }
//
//            @Override
//            public void onCancelled(DatabaseError databaseError) {
//
//            }
//        });
//    }

    private void loadCollectionMenu(final Context context, final Callback callback) {


        RealmController realmController = new RealmController(context);


        retrieveMedia_Collection(realmController.CollectionMenu(), context);

        if (callback != null) {
            callback.onMusicCatalogReady(mCurrentState == State.INITIALIZED);
        }




    }

    private void loadCollectionMenuCon(final Context context, final Callback callback, String colecID) {


        RealmController realmController = new RealmController(context);


        retrieveMedia_Collection(realmController.CollectionMenuCon(colecID), context);

        if (callback != null) {
            callback.onMusicCatalogReady(mCurrentState == State.INITIALIZED);
        }







    }

    private void loadCollectionMenuTopic(final Context context, final Callback callback, String toppicID) {


        RealmController realmController = new RealmController(context);


        retrieveMedia_Collection(realmController.CollectionMenuTopic(toppicID), context);

        if (callback != null) {
            callback.onMusicCatalogReady(mCurrentState == State.INITIALIZED);
        }


    }

    private void loadSongCollection(final Context context, final Callback callback, String CollectionID) {


        RealmController realmController = new RealmController(context);


        retrieveMedia_Collection(realmController.getsongCollection(CollectionID), context);

        if (callback != null) {
            callback.onMusicCatalogReady(mCurrentState == State.INITIALIZED);
        }


    }


    private synchronized void retrieveMedia_Playlist(ArrayList<MediaMetadataCompat> list, Context context) {
        try {
            if (mCurrentState == State.NON_INITIALIZED) {
                mCurrentState = State.INITIALIZING;

                Iterator<MediaMetadataCompat> tracks = list.iterator();
                while (tracks.hasNext()) {
                    MediaMetadataCompat item = tracks.next();
                    String musicId = item.getString(MediaMetadataCompat.METADATA_KEY_MEDIA_ID);
                    mMusicListById.put(musicId, new MutableMediaMetadata(musicId, item));
                }
                buildListsByGenre();
                mCurrentState = State.INITIALIZED;
            }
        } finally {
            if (mCurrentState != State.INITIALIZED) {
                // Something bad happened, so we reset state to NON_INITIALIZED to allow
                // retries (eg if the network connection is temporary unavailable)
                mCurrentState = State.NON_INITIALIZED;
            }
        }
    }

    private synchronized void retrieveMedia_Collection(ArrayList<MediaMetadataCompat> list, Context context) {
        try {
            if (mCurrentState == State.NON_INITIALIZED) {
                mCurrentState = State.INITIALIZING;

                Iterator<MediaMetadataCompat> tracks = list.iterator();
                while (tracks.hasNext()) {
                    MediaMetadataCompat item = tracks.next();
                    String musicId = item.getString(MediaMetadataCompat.METADATA_KEY_MEDIA_ID);
                    mMusicListById.put(musicId, new MutableMediaMetadata(musicId, item));
                }
                buildListsByGenre();
                mCurrentState = State.INITIALIZED;
            }
        } finally {
            if (mCurrentState != State.INITIALIZED) {
                // Something bad happened, so we reset state to NON_INITIALIZED to allow
                // retries (eg if the network connection is temporary unavailable)
                mCurrentState = State.NON_INITIALIZED;
            }
        }
    }

    private synchronized void retrieveMedia_Top_k(Context context) {

        RealmController realmController = new RealmController(context);

        try {
            if (mCurrentState == State.NON_INITIALIZED) {
                mCurrentState = State.INITIALIZING;

                Iterator<MediaMetadataCompat> tracks = realmController.Tophit_KPop().iterator();
                while (tracks.hasNext()) {
                    MediaMetadataCompat item = tracks.next();
                    String musicId = item.getString(MediaMetadataCompat.METADATA_KEY_MEDIA_ID);
                    mMusicListById.put(musicId, new MutableMediaMetadata(musicId, item));
                }
                buildListsByGenre();
                mCurrentState = State.INITIALIZED;
            }
        } finally {
            if (mCurrentState != State.INITIALIZED) {
                // Something bad happened, so we reset state to NON_INITIALIZED to allow
                // retries (eg if the network connection is temporary unavailable)
                mCurrentState = State.NON_INITIALIZED;
            }
        }
    }

    private synchronized void retrieveMedia_Top_us(Context context) {

        RealmController realmController = new RealmController(context);
        try {
            if (mCurrentState == State.NON_INITIALIZED) {
                mCurrentState = State.INITIALIZING;

                Iterator<MediaMetadataCompat> tracks = realmController.Tophit_US().iterator();
                while (tracks.hasNext()) {
                    MediaMetadataCompat item = tracks.next();
                    String musicId = item.getString(MediaMetadataCompat.METADATA_KEY_MEDIA_ID);
                    mMusicListById.put(musicId, new MutableMediaMetadata(musicId, item));
                }
                buildListsByGenre();
                mCurrentState = State.INITIALIZED;
            }
        } finally {
            if (mCurrentState != State.INITIALIZED) {
                // Something bad happened, so we reset state to NON_INITIALIZED to allow
                // retries (eg if the network connection is temporary unavailable)
                mCurrentState = State.NON_INITIALIZED;
            }
        }
    }

    public List<MediaBrowserCompat.MediaItem> getChildren_search(String mediaId, Resources resources) {

        List<MediaBrowserCompat.MediaItem> mediaItems = new ArrayList<>();

        if (!MediaIDHelper.isBrowseable(mediaId)) {
            return mediaItems;
        }

        if (constants.SEARCH_ID.equals(mediaId)) {

            for (MediaMetadataCompat metadata : getShuffledMusic()) {
                mediaItems.add(createMediaItem(metadata));
            }

        } else if (constants.TOPHIT_VID.equals(mediaId)) {

            for (MediaMetadataCompat metadata : getShuffledMusic()) {
                mediaItems.add(createMediaItem(metadata));
            }


        } else if (MEDIA_ID_MUSICS_BY_PLAYLIST_NAME.equals(mediaId)) {
            int i = 0;
            for (String genre : getGenres()) {
                mediaItems.add(createBrowsableMediaItemForGenre(genre, resources));
                i++;
            }

        } else if (mediaId.startsWith(MEDIA_ID_MUSICS_BY_GENRE)) {
            String genre = MediaIDHelper.getHierarchy(mediaId)[1];
            for (MediaMetadataCompat metadata : getMusicsByGenre(genre)) {
                mediaItems.add(createMediaItem(metadata));
            }


        } else {
            for (MediaMetadataCompat metadata : getShuffledMusic()) {
                mediaItems.add(createMediaItem(metadata));
            }
        }

        return mediaItems;
    }

    public List<MediaBrowserCompat.MediaItem> getChildren(String mediaId, Resources resources) {
        List<MediaBrowserCompat.MediaItem> mediaItems = new ArrayList<>();

        if (!MediaIDHelper.isBrowseable(mediaId)) {
            return mediaItems;
        }

      //  mediaItems.add(getGenres(metadata));



        if (MEDIA_ID_ROOT.equals(mediaId)) {

            for (MediaMetadataCompat metadata : getShuffledMusic()) {
                mediaItems.add(createMediaItem(metadata));
            }


        } else if (constants.SEARCH_ID.equals(mediaId)) {

            for (MediaMetadataCompat metadata : getShuffledMusic()) {
                mediaItems.add(createMediaItem(metadata));
            }

        } else if (constants.TOPHIT_VID.equals(mediaId)) {

            for (MediaMetadataCompat metadata : getShuffledMusic()) {
                mediaItems.add(createMediaItem(metadata));
            }


        } else if (MEDIA_ID_MUSICS_BY_PLAYLIST_NAME.equals(mediaId)) {

            for (String genre : getGenres()) {
                mediaItems.add(createBrowsableMediaItemForGenre_Playlist(genre, resources));

            }

        } else if (mediaId.equals(COLLECTION_MENU_TOPIC)) {
            for (MediaMetadataCompat metadata : getShuffledMusic()) {
                mediaItems.add(createMediaItem(metadata));
            }

        } else if (mediaId.startsWith(MEDIA_ID_MUSICS_BY_GENRE)) {
            String genre = MediaIDHelper.getHierarchy(mediaId)[1];
            for (MediaMetadataCompat metadata : getMusicsByGenre(genre)) {
                mediaItems.add(createMediaItem(metadata));
            }
        } else if (mediaId.startsWith(COLLECTION_MENU_TOPIC)) {
            for (MediaMetadataCompat metadata : getShuffledMusic()) {
                mediaItems.add(createBrowsableMediaItemForCollectionTopic(metadata));

            }
        } else if (mediaId.startsWith(COLLECTION_MENU_CON)) {
            for (MediaMetadataCompat metadata : getShuffledMusic()) {
                mediaItems.add(createBrowsableMediaItemForCollectionTopic(metadata));

            }
        } else if (mediaId.startsWith(COLLECTION_MENU)) {
            for (MediaMetadataCompat metadata : getShuffledMusic()) {
                mediaItems.add(createBrowsableMediaItemForCollectionCon(metadata));

            }
        } else if (mediaId.startsWith(COLLECTION_)) {
            for (MediaMetadataCompat metadata : getShuffledMusic()) {
                mediaItems.add(createBrowsableMediaItemForCollection(metadata));

            }
        } else {
            for (MediaMetadataCompat metadata : getShuffledMusic()) {
                mediaItems.add(createMediaItem(metadata));
            }
        }

        return mediaItems;
    }

    public List<MediaBrowserCompat.MediaItem> getChildrenPlaylist(String mediaId, Resources resources) {
        List<MediaBrowserCompat.MediaItem> mediaItems = new ArrayList<>();

        if (!MediaIDHelper.isBrowseable(mediaId)) {
            return mediaItems;
        }


        if (MEDIA_ID_MUSICS_BY_PLAYLIST_NAME.equals(mediaId)) {
            for (String genre : getGenres()) {
                mediaItems.add(createBrowsableMediaItemForGenre(genre, resources));
            }

        } else if (mediaId.startsWith(MEDIA_ID_MUSICS_BY_GENRE)) {
            String genre = MediaIDHelper.getHierarchy(mediaId)[1];
            for (MediaMetadataCompat metadata : getMusicsByGenre(genre)) {
                mediaItems.add(createMediaItem(metadata));
            }

        } else {
            LogHelper.w(TAG, "Skipping unmatched mediaId: ", mediaId);
        }
        return mediaItems;
    }

    private MediaBrowserCompat.MediaItem createBrowsableMediaItemForRoot(Resources resources) {
        MediaDescriptionCompat description = new MediaDescriptionCompat.Builder()
                .setMediaId(MEDIA_ID_MUSICS_BY_GENRE)
                .setTitle(resources.getString(R.string.browse_genres))
                .setSubtitle(resources.getString(R.string.browse_genre_subtitle))
                .setIconUri(Uri.parse("android.resource://" +
                        "com.example.android.uamp/drawable/ic_by_genre"))
                .build();


        return new MediaBrowserCompat.MediaItem(description,
                MediaBrowserCompat.MediaItem.FLAG_BROWSABLE);
    }

    private MediaBrowserCompat.MediaItem createBrowsableMediaItemForCollection(MediaMetadataCompat item) {

        MediaDescriptionCompat description = new MediaDescriptionCompat.Builder()
                .setMediaId(createMediaID(null, COLLECTION_MENU, item.getDescription().getMediaId()))
                .setTitle(item.getDescription().getTitle())
                .setIconUri(item.getDescription().getIconUri())

                .build();

        return new MediaBrowserCompat.MediaItem(description,
                MediaBrowserCompat.MediaItem.FLAG_BROWSABLE);
    }

    private MediaBrowserCompat.MediaItem createBrowsableMediaItemForCollectionCon(MediaMetadataCompat item) {


        MediaDescriptionCompat description = new MediaDescriptionCompat.Builder()
                .setMediaId(createMediaID(null, COLLECTION_MENU_CON, item.getDescription().getMediaId()))
                .setTitle(item.getDescription().getTitle())
                .setIconUri(item.getDescription().getIconUri())

                .build();


        return new MediaBrowserCompat.MediaItem(description,
                MediaBrowserCompat.MediaItem.FLAG_BROWSABLE);
    }

    private MediaBrowserCompat.MediaItem createBrowsableMediaItemForCollectionTopic(MediaMetadataCompat item) {


        MediaDescriptionCompat description = new MediaDescriptionCompat.Builder()
                .setMediaId(createMediaID(null, COLLECTION_MENU_TOPIC, item.getDescription().getTitle().toString()))
                .setTitle(item.getDescription().getTitle())
                .setIconUri(item.getDescription().getIconUri())

                .build();


        return new MediaBrowserCompat.MediaItem(description,
                MediaBrowserCompat.MediaItem.FLAG_BROWSABLE);
    }
    private MediaBrowserCompat.MediaItem createBrowsableMediaItemForGenre(String genre,
                                                                    Resources resources) {
        String url = "http://image.mp3.zdn.vn//thumb/240_240/avatars/e/a/eac7b6df395627214cd3b7900416eccc_1441251753.jpg";
        Uri icon = Uri.parse(url);
        MediaDescriptionCompat description = new MediaDescriptionCompat.Builder()
                .setMediaId(createMediaID(null, MEDIA_ID_MUSICS_BY_GENRE, genre))
                .setTitle(genre)
                .setIconUri(icon)
                .setSubtitle(resources.getString(
                        R.string.browse_musics_by_genre_subtitle, genre))
                .build();

        return new MediaBrowserCompat.MediaItem(description,
                MediaBrowserCompat.MediaItem.FLAG_BROWSABLE);
    }


    private MediaBrowserCompat.MediaItem createBrowsableMediaItemForGenre_Playlist(String genre,
                                                                                   Resources resources) {
        String url = "http://image.mp3.zdn.vn//thumb/240_240/avatars/e/a/eac7b6df395627214cd3b7900416eccc_1441251753.jpg";
        Uri icon = Uri.parse(url);
        MediaDescriptionCompat description = new MediaDescriptionCompat.Builder()
                .setMediaId(createMediaID(null, MEDIA_ID_MUSICS_BY_GENRE, genre))
                .setTitle(genre)
                .setIconUri(icon)
                .setSubtitle(resources.getString(
                        R.string.browse_musics_by_genre_subtitle, NumberSong.get(genre.toString()).toString()))
                .build();

        return new MediaBrowserCompat.MediaItem(description,
                MediaBrowserCompat.MediaItem.FLAG_BROWSABLE);
    }

    public static MediaBrowserCompat.MediaItem createMediaItem(MediaMetadataCompat metadata) {
        // Since mediaMetadata fields are immutable, we need to create a copy, so we
        // can set a hierarchy-aware mediaID. We will need to know the media hierarchy
        // when we get a onPlayFromMusicID call, so we can create the proper queue based
        // on where the music was selected from (by artist, by genre, random, etc)

//
        String genre = metadata.getString(MediaMetadataCompat.METADATA_KEY_GENRE);


        String hierarchyAwareMediaID = MediaIDHelper.createMediaID(
                metadata.getDescription().getMediaId(), MEDIA_ID_MUSICS_BY_GENRE, genre);


        Long duration = metadata.getLong(MediaMetadataCompat.METADATA_KEY_DURATION);
        String source = metadata.getString(MusicProviderSource.CUSTOM_METADATA_TRACK_SOURCE);
        String id_song = metadata.getString(MediaMetadataCompat.METADATA_KEY_MEDIA_ID);
        String icon_url = metadata.getString(MediaMetadataCompat.METADATA_KEY_DISPLAY_ICON_URI);
        Bundle songDuration = new Bundle();
        songDuration.putLong(MediaMetadataCompat.METADATA_KEY_DURATION, duration);
        songDuration.putString(MediaIDHelper.SOURCE_SONG, source);
        songDuration.putString(MediaIDHelper.ID_SONG, id_song);
        songDuration.putString(MediaIDHelper.ICON_SONG, icon_url);
        songDuration.putString(MediaIDHelper.GENRE_SONG, genre);




        MediaMetadataCompat copy = new MediaMetadataCompat.Builder(metadata)
                .putString(MediaMetadataCompat.METADATA_KEY_MEDIA_ID, hierarchyAwareMediaID)
                .build();


        MediaDescriptionCompat description = new MediaDescriptionCompat.Builder()
                .setExtras(songDuration)
                .setMediaId(copy.getDescription().getMediaId())
                .setTitle(copy.getDescription().getTitle())
                .setIconUri(copy.getDescription().getIconUri())
                .setSubtitle(copy.getDescription().getSubtitle())

                .build();

        return new MediaBrowserCompat.MediaItem(description,
                MediaBrowserCompat.MediaItem.FLAG_PLAYABLE);

    }
    private MediaBrowserCompat.MediaItem createAllMediaItem(String id) {
        // Since mediaMetadata fields are immutable, we need to create a copy, so we
        // can set a hierarchy-aware mediaID. We will need to know the media hierarchy
        // when we get a onPlayFromMusicID call, so we can create the proper queue based
        // on where the music was selected from (by artist, by genre, random, etc)


        MediaMetadataCompat copy = new MediaMetadataCompat.Builder()
                .putString(MediaMetadataCompat.METADATA_KEY_MEDIA_ID, id)
                .build();
        return new MediaBrowserCompat.MediaItem(copy.getDescription(),
                MediaBrowserCompat.MediaItem.FLAG_PLAYABLE);

    }
    public void setNewSource(MusicProviderSource source) {
        mSource = source;
        mCurrentState = State.NON_INITIALIZED;
        mMusicListByGenre = new ConcurrentHashMap<>();
        mMusicListById = new ConcurrentHashMap<>();
        mFavoriteTracks = Collections.newSetFromMap(new ConcurrentHashMap<String, Boolean>());
    }


}
