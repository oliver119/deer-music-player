package com.deer.musicplayer.Dataset;


import com.deer.musicplayer.Enity.User_Enity;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

/**
 * Created by User on 6/16/2017.
 */

public class Add_Dataset {
    private DatabaseReference mDatabase, UserRef;


    DatabaseReference database = FirebaseDatabase.getInstance().getReference();


    public void Add_New_User(String userId, String name, String email, String country, String phone, String type, String Avatar_URL) {
        database = FirebaseDatabase.getInstance().getReference();
        User_Enity user = new User_Enity(userId, email, country, name, phone, type, Avatar_URL);

        database.child("User").child(userId).child("userInfo").setValue(user);

        database.child("User").child(userId).child("userInfo").child("online").setValue(false);
        database.child("User").child(userId).child("playlist").child("online").setValue(false);


    }

    public void add_playlist(String playlistname, String song_id, String name, String duration, String allbum, String artist, String source, String icon) {
        DatabaseReference database = FirebaseDatabase.getInstance().getReference().child("User").child(FirebaseAuth.getInstance().getCurrentUser().getUid()).child("playlist").child(playlistname).child("song");

        database.child(song_id).child("name_song").setValue(name);
        database.child(song_id).child("duration_song").setValue(duration);
        database.child(song_id).child("allbum_song").setValue(allbum);
        database.child(song_id).child("artist_song").setValue(artist);
        database.child(song_id).child("source_song").setValue(source);
        database.child(song_id).child("icon_song").setValue(icon);


    }

    public void move_playlist(String playlistname, String song_id, String name, String duration, String allbum, String artist, String source, String icon, String genner) {

        DatabaseReference databasedell = FirebaseDatabase.getInstance().getReference().child("User").child(FirebaseAuth.getInstance().getCurrentUser().getUid()).child("playlist").child(genner).child("song").child(song_id);

        databasedell.removeValue();

        DatabaseReference database = FirebaseDatabase.getInstance().getReference().child("User").child(FirebaseAuth.getInstance().getCurrentUser().getUid()).child("playlist").child(playlistname).child("song");

        database.child(song_id).child("name_song").setValue(name);
        database.child(song_id).child("duration_song").setValue(duration);
        database.child(song_id).child("allbum_song").setValue(allbum);
        database.child(song_id).child("artist_song").setValue(artist);
        database.child(song_id).child("source_song").setValue(source);
        database.child(song_id).child("icon_song").setValue(icon);


    }


}
